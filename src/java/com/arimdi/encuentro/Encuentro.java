/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro;

import com.arimdi.encuentro.impl.EBrowserNode;
import com.arimdi.encuentro.impl.ENode;
import com.arimdi.encuentro.service.NetworkServices;
import com.arimdi.encuentro.model.NetworkServicesBrowser;
import com.arimdi.encuentro.crypto.Cipher;
import com.arimdi.encuentro.util.RandomGenerator;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Singleton type class as entry point for the <b>Encuentro</b>
 * library.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public final class Encuentro {

  private static InetAddress c_MulticastAddress;
  private static InetAddress c_LocalAddress;
  private static RandomGenerator c_RNG;
  private static NetworkServices c_NetworkServices;
  private static NetworkServicesBrowser c_NetworkServicesBrowser;
  private static int c_MTU;
  private static int c_MaxPayload;
  public static boolean debug = false;
  public static boolean secure =
      "true".equals(System.getProperty("com.arimdi.encuentro.secure"));
  private static Cipher c_Cipher;

  static {
    initRandom();
    debug = "true".equals(System.getProperty("com.arimdi.encuentro.debug"));
    if (secure) {
      String key = System.getProperty("com.arimdi.encuentro.secure.key");
      if (key == null) {
        key = "";
      }
      c_Cipher = new Cipher(key);
    }
    String mtu = System.getProperty("com.arimdi.encuentro.mtu");
    if (mtu == null || mtu.length() == 0) {
      c_MTU = 1400;
    } else {
      c_MTU = Integer.parseInt(mtu);
    }

    c_MaxPayload = c_MTU - 52; //Header + Space for padding & encryption
    if (debug) System.out.println("Encuentro: Debug messages activated.");
    if (debug) System.out.println("Encuentro MTU = " + c_MTU);
  }//static intializer

  /**
   * Constructs a <tt>Encuentro</tt> instance.
   * Private to make this a singleton.
   */
  private Encuentro() {
  }//constructor

  /**
   * Returns the <tt>NetworkServices</tt> instance
   * within the singleton context.
   *
   * @return a <tt>NetworkServices</tt> instance.
   */
  public static final NetworkServices getNetworkServices() {
    if (c_NetworkServices == null) {
      c_NetworkServices = new ENode();
    }
    return c_NetworkServices;
  }//getNetworkServices

  /**
   * Returns the <tt>NetworServicesBrowser</tt> instance
   * within the singleton context.
   *
   * @return a <tt>NetworkServicesBrowser</tt> instance.
   */
  public static final NetworkServicesBrowser getNetworkServicesBrowser() {
    if (c_NetworkServicesBrowser == null) {
      c_NetworkServicesBrowser = new EBrowserNode();
    }
    return c_NetworkServicesBrowser;
  }//getNetworkServicesBrowser

  /**
   * Returns the administrative multicast address.
   *
   * @return the administrative multicast
   *         <tt>InetAddress</tt> instance.
   */
  public static final InetAddress getMulticastAddress() {
    if (c_MulticastAddress == null) {
      try {
        c_MulticastAddress = InetAddress.getByName(ADMIN_MULTICAST);
      } catch (UnknownHostException ex) {
        c_MulticastAddress = null;
      }
    }
    return c_MulticastAddress;
  }//getMulticastAddress


  public static final void setLocalAddress(InetAddress addr)
    throws Exception {

    c_LocalAddress = addr;

    //@commentstart@
    InetAddress[] add = InetAddress.getAllByName("localhost");
    for (int i = 0; i < add.length; i++) {
      InetAddress inetAddress = add[i];
      if(inetAddress.equals(addr)) {
        c_LocalAddress = addr;
        return;
      }
    }
    throw new IllegalArgumentException("IP not local.");
    //@commentend@
  }//setLocalAddress

  /**
   * Returns the local IP address.
   *
   * @return the local address as <tt>InetAddress</tt> instance.
   */
  public static final InetAddress getLocalAddress() {
    if (c_LocalAddress == null) {
      try {
        c_LocalAddress = InetAddress.getLocalHost();
      } catch (UnknownHostException ex) {
        c_LocalAddress = null;
      }
    }
    return c_LocalAddress;
  }//getLocalAddress

  public static final Cipher getCipher() {
    return c_Cipher;
  }//getCipher

  /**
   * Returns a random integer identifier.
   *
   * @return a random <tt>int</tt>.
   */
  public static final int getRandomIdentifier() {
    return c_RNG.nextShort();
  }//getRandomIdentifier

  public static final int getRandomInt() {
    return c_RNG.nextInt();
  }//getRandomInt

  /**
   * Returns the defined maximum transport package size.
   * <p/>
   * The default is 1400 bytes, which can be set
   * via the system property <tt>com.arimdi.encuentro.mtu</tt>.
   *
   * @return the MTU as <tt>int</tt>
   */
  public static final int getMTU() {
    return c_MTU;
  }//getMTU

  /**
   * Returns the maximum transport package payload
   * size.
   * <p/>
   * The maximum payload is equal to the MTU minus the
   * IP and UDP header sizes (approx. 32 bytes).
   *
   * @return the maximum payload size as <tt>int</tt>.
   */
  public static final int getMaxPayload() {
    return c_MaxPayload;
  }//getMaxPayload

  /**
   * Initializes the random number generator.
   */
  private static final void initRandom() {
    c_RNG = new RandomGenerator(toInt(getLocalAddress().getAddress()));
  }//initRandom

  /**
   * Converts the given raw byte[4] into an int value.
   *
   * @param bytes the raw bytes.
   * @return the value assembled from the given bytes.
   */
  public static final int toInt(byte[] bytes) {
    return (
        ((bytes[0] & 0xff) << 24) |
        ((bytes[1] & 0xff) << 16) |
        ((bytes[2] & 0xff) << 8) |
        (bytes[3] & 0xff)
        );
  }//toInt

  public static final byte[] toBytes(int v) {
    byte[] bytes = new byte[4];
    bytes[0] = (byte)(0xff & (v >> 24));
    bytes[1] = (byte)(0xff & (v >> 16));
    bytes[2] = (byte)(0xff & (v >>  8));
    bytes[3] = (byte)(0xff & v);
    return bytes;
  }//toBytes

  public static final boolean reconfigure(boolean debug,int mtu,boolean secure,String key, boolean announce) {   
    Encuentro.debug = debug;
    c_MTU = mtu;
    c_MaxPayload = c_MTU - 48; //header and paddingspace
    Encuentro.getNetworkServices().setAnnouncing(announce);
    try {
      if(c_Cipher == null) {
        c_Cipher = new Cipher(key);
      } else {
        c_Cipher.setKey(key);
      }
      Encuentro.secure=secure;
      return true;
    } catch (Exception ex) {
      return false;
    }

  }//reconfigure

  /**
   * Defines the maximum size of an unsigned short value.
   */
  public static final int UNSIGNED_SHORT_MAX = Short.MAX_VALUE - Short.MIN_VALUE;

  /**
   * Defined the <tt>Encuentro</tt> protocol port number.
   */
  public static final int PORT = 4251;

  /**
   * Defines the administrative multicast IP address as <tt>String</tt>.
   */
  public static final String ADMIN_MULTICAST = "224.0.4.251";

  /**
   * Defines a default discovery timeout.
   */
  public static final int DISCOVERY_TIMEOUT = 2500;

  /**
   * Defines standard operation mode for Encuentro service
   * operation with regards to announcement of service registration
   * and deregistration.
   * (default = true)
   */
  public static final boolean ANNOUNCE_REGISTRATION = true;

}//class Encuentro
