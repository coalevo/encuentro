/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.util;

/**
 * A synchronization aid that allows one or more threads to wait until
 * a set of operations being performed in other threads completes.
 * <p/>
 * <p>A <tt>CountDownLatch</tt> is initialized with a given
 * <em>count</em>.  The {@link #await await} methods block until the current
 * {@link #getCount count} reaches zero due to invocations of the
 * {@link #countDown} method, after which all waiting threads are
 * released and any subsequent invocations of {@link #await await} return
 * immediately. This is a one-shot phenomenon -- the count cannot be
 * reset.
 * <p/>
 * <p>A <tt>CountDownLatch</tt> is a versatile synchronization tool
 * and can be used for a number of purposes.  A
 * <tt>CountDownLatch</tt> initialized with a count of one serves as a
 * simple on/off latch, or gate: all threads invoking {@link #await await}
 * wait at the gate until it is opened by a thread invoking {@link
 * #countDown}.  A <tt>CountDownLatch</tt> initialized to <em>N</em>
 * can be used to make one thread wait until <em>N</em> threads have
 * completed some action, or some action has been completed N times.
 * <p>A useful property of a <tt>CountDownLatch</tt> is that it
 * doesn't require that threads calling <tt>countDown</tt> wait for
 * the count to reach zero before proceeding, it simply prevents any
 * thread from proceeding past an {@link #await await} until all
 * threads could pass.
 * <p/>
 * <p><b>Sample usage:</b> Here is a pair of classes in which a group
 * of worker threads use two countdown latches:
 * <ul>
 * <li>The first is a start signal that prevents any worker from proceeding
 * until the driver is ready for them to proceed;
 * <li>The second is a completion signal that allows the driver to wait
 * until all workers have completed.
 * </ul>
 * <p/>
 * <pre>
 * class Driver { // ...
 *   void main() throws InterruptedException {
 *     CountDownLatch startSignal = new CountDownLatch(1);
 *     CountDownLatch doneSignal = new CountDownLatch(N);
 * <p/>
 *     for (int i = 0; i < N; ++i) // create and start threads
 *       new Thread(new Worker(startSignal, doneSignal)).start();
 * <p/>
 *     doSomethingElse();            // don't let run yet
 *     startSignal.countDown();      // let all threads proceed
 *     doSomethingElse();
 *     doneSignal.await();           // wait for all to finish
 *   }
 * }
 * <p/>
 * class Worker implements Runnable {
 *   private final CountDownLatch startSignal;
 *   private final CountDownLatch doneSignal;
 *   Worker(CountDownLatch startSignal, CountDownLatch doneSignal) {
 *      this.startSignal = startSignal;
 *      this.doneSignal = doneSignal;
 *   }
 *   public void run() {
 *      try {
 *        startSignal.await();
 *        doWork();
 *        doneSignal.countDown();
 *      } catch (InterruptedException ex) {} // return;
 *   }
 * <p/>
 *   void doWork() { ... }
 * }
 * <p/>
 * </pre>
 * <p/>
 * <p>Another typical usage would be to divide a problem into N parts,
 * describe each part with a Runnable that executes that portion and
 * counts down on the latch, and queue all the Runnables to an
 * Executor.  When all sub-parts are complete, the coordinating thread
 * will be able to pass through await.
 * <p/>
 * <pre>
 * class Driver2 { // ...
 *   void main() throws InterruptedException {
 *     CountDownLatch doneSignal = new CountDownLatch(N);
 *     Executor e = ...
 * <p/>
 *     for (int i = 0; i < N; ++i) // create and start threads
 *       e.execute(new WorkerRunnable(doneSignal, i));
 * <p/>
 *     doneSignal.await();           // wait for all to finish
 *   }
 * }
 * <p/>
 * class WorkerRunnable implements Runnable {
 *   private final CountDownLatch doneSignal;
 *   private final int i;
 *   WorkerRunnable(CountDownLatch doneSignal, int i) {
 *      this.doneSignal = doneSignal;
 *      this.i = i;
 *   }
 *   public void run() {
 *      try {
 *        doWork(i);
 *        doneSignal.countDown();
 *      } catch (InterruptedException ex) {} // return;
 *   }
 * <p/>
 *   void doWork() { ... }
 * }
 * <p/>
 * </pre>
 *
 * @author Doug Lea
 */
public class CountDownLatch {

  private int count_;

  /**
   * Constructs a <tt>CountDownLatch</tt> initialized with the given
   * count.
   *
   * @param count the number of times {@link #countDown} must be invoked
   *              before threads can pass through {@link #await}.
   * @throws IllegalArgumentException if <tt>count</tt> is less than zero.
   */
  public CountDownLatch(int count) {
    if (count < 0) throw new IllegalArgumentException("count < 0");
    this.count_ = count;
  }//constructor

  /**
   * Causes the current thread to wait until the latch has counted down to
   * zero, unless the thread is {@link Thread#interrupt interrupted}.
   * <p/>
   * <p>If the current {@link #getCount count} is zero then this method
   * returns immediately.
   * <p>If the current {@link #getCount count} is greater than zero then
   * the current thread becomes disabled for thread scheduling
   * purposes and lies dormant until one of two things happen:
   * <ul>
   * <li>The count reaches zero due to invocations of the
   * {@link #countDown} method; or
   * <li>Some other thread {@link Thread#interrupt interrupts} the current
   * thread.
   * </ul>
   * <p>If the current thread:
   * <ul>
   * <li>has its interrupted status set on entry to this method; or
   * <li>is {@link Thread#interrupt interrupted} while waiting,
   * </ul>
   * then {@link InterruptedException} is thrown and the current thread's
   * interrupted status is cleared.
   *
   * @throws InterruptedException if the current thread is interrupted
   *                              while waiting.
   */
  public void await() throws InterruptedException {
    if (Thread.interrupted()) throw new InterruptedException();
    synchronized (this) {
      while (count_ > 0)
        wait();
    }
  }//await

  /**
   * Causes the current thread to wait until the latch has counted down to
   * zero, unless the thread is {@link Thread#interrupt interrupted},
   * or the specified waiting time elapses.
   * <p/>
   * <p>If the current {@link #getCount count} is zero then this method
   * returns immediately with the value <tt>true</tt>.
   * <p/>
   * <p>If the current {@link #getCount count} is greater than zero then
   * the current thread becomes disabled for thread scheduling
   * purposes and lies dormant until one of three things happen:
   * <ul>
   * <li>The count reaches zero due to invocations of the
   * {@link #countDown} method; or
   * <li>Some other thread {@link Thread#interrupt interrupts} the current
   * thread; or
   * <li>The specified waiting time elapses.
   * </ul>
   * <p>If the count reaches zero then the method returns with the
   * value <tt>true</tt>.
   * <p>If the current thread:
   * <ul>
   * <li>has its interrupted status set on entry to this method; or
   * <li>is {@link Thread#interrupt interrupted} while waiting,
   * </ul>
   * then {@link InterruptedException} is thrown and the current thread's
   * interrupted status is cleared.
   * <p/>
   * <p>If the specified waiting time elapses then the value <tt>false</tt>
   * is returned.
   * If the time is
   * less than or equal to zero, the method will not wait at all.
   *
   * @param timeout the maximum time to wait in milliseconds
   * @return <tt>true</tt> if the count reached zero  and <tt>false</tt>
   *         if the waiting time elapsed before the count reached zero.
   * @throws InterruptedException if the current thread is interrupted
   *                              while waiting.
   */
  public boolean await(long timeout) throws InterruptedException {
    if (Thread.interrupted()) throw new InterruptedException();
    long millis = timeout;

    synchronized (this) {
      if (count_ <= 0)
        return true;
      else if (millis <= 0)
        return false;
      else {
        long deadline = System.currentTimeMillis() + millis;
        for (; ;) {
          this.wait(millis);

          if (count_ <= 0) {
            return true;
          } else {
            millis = deadline - System.currentTimeMillis();
            if (millis <= 0) {
              return false;
            }
          }
        }
      }
    }
  }//await

  /**
   * Decrements the count of the latch, releasing all waiting threads if
   * the count reaches zero.
   * <p>If the current {@link #getCount count} is greater than zero then
   * it is decremented. If the new count is zero then all waiting threads
   * are re-enabled for thread scheduling purposes.
   * <p>If the current {@link #getCount count} equals zero then nothing
   * happens.
   */
  public synchronized void countDown() {
    if (count_ == 0) return;
    if (--count_ == 0)
      notifyAll();
  }//countDown

  /**
   * Returns the current count.
   * <p>This method is typically used for debugging and testing purposes.
   *
   * @return the current count.
   */
  public long getCount() {
    return count_;
  }//getCount

  /**
   * Returns a string identifying this latch, as well as its state.
   * The state, in brackets, includes the String
   * &quot;Count =&quot; followed by the current count.
   *
   * @return a string identifying this latch, as well as its
   *         state
   */
  public String toString() {
    return super.toString() + "[Count = " + getCount() + "]";
  }//toString

}//CountDownLatch
