/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.util;

/**
 * Utility class for working with String instances.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public final class StringUtil {

  /**
   * Returns the length of a String object as the
   * number of bytes when written as UTF using a
   * DataOutput instance.<p>
   * When written to UTF, two bytes are prefixed, which
   * denote the length of bytes that follow as an unsigned
   * short value.
   *
   * @param str the string to be length checked.
   * @return the length of the given string.
   * @see java.io.DataOutput#writeUTF(String)
   */
  public static final int getUTFOutputLength(String str) {
    int l = str.length();
    int utfl = 2;
    for (int n = 0; n < l; n++) {
      char c = str.charAt(n);
      if (c > 0x07FF) {
        utfl += 3;
      } else if ((c > 0x0000) && (c < 0x0080)) {
        utfl++;
      } else {
        utfl += 2;
      }
    }
    return utfl;
  }//getUTFLength

}//class StringUtil
