/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.util;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Vector;

/**
 * Provides an utility for resolving local addresses.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class NetworkUtility {

  public static InetAddress[] getLocalAddresses()
      throws Exception {
    Vector v = new Vector();
    for (Enumeration e = NetworkInterface.getNetworkInterfaces(); e.hasMoreElements();) {
      NetworkInterface ni = (NetworkInterface) e.nextElement();
      for (Enumeration e2 = ni.getInetAddresses(); e2.hasMoreElements();) {
        v.addElement(e2.nextElement());
      }
    }
    InetAddress[] addrs = new InetAddress[v.size()];
    for (int i = 0; i < addrs.length; i++) {
      addrs[i] = (InetAddress) v.elementAt(i);
    }
    return addrs;
  }//getLocalAddresses

  /**
   * Returns the IPv4 Link Local Local host address (first found).
   *
   * @return an InetAddress[].
   * @throws Exception if the interfaces cannot be obtained, or their addresses.
   */
  public static InetAddress getLinkLocalHost() throws Exception {
    for (Enumeration e = NetworkInterface.getNetworkInterfaces(); e.hasMoreElements();) {
      NetworkInterface ni = (NetworkInterface) e.nextElement();
      for (Enumeration e2 = ni.getInetAddresses(); e2.hasMoreElements();) {
        InetAddress addr = (InetAddress) e2.nextElement();
        if (addr instanceof Inet4Address) {
          //check first bytes for link local
          byte[] addrbytes = addr.getAddress();
          //System.out.println(Arrays.toString(addrbytes));

          if ((addrbytes[0] & 0xff) == 169 && (addrbytes[1] & 0xff) == 254) {
            return addr;
          }

        }
      }
    }
    return null;
  }//getLinkLocalHost

  /**
   * Returns IPv4 Link Local Local host addresses.
   *
   * @return an InetAddress[].
   * @throws Exception if the interfaces cannot be obtained, or their addresses.
   */
  public static InetAddress[] getIP4LLAddresses()
      throws Exception {
    Vector v = new Vector();
    for (Enumeration e = NetworkInterface.getNetworkInterfaces(); e.hasMoreElements();) {
      NetworkInterface ni = (NetworkInterface) e.nextElement();
      for (Enumeration e2 = ni.getInetAddresses(); e2.hasMoreElements();) {
        InetAddress addr = (InetAddress) e2.nextElement();
        if (addr instanceof Inet4Address) {
          //check first bytes for link local
          byte[] addrbytes = addr.getAddress();
          //System.out.println(Arrays.toString(addrbytes));

          if ((addrbytes[0] & 0xff) == 169 && (addrbytes[1] & 0xff) == 254) {
            v.addElement(addr);
          }

        }
      }
    }
    InetAddress[] addrs = new InetAddress[v.size()];
    for (int i = 0; i < addrs.length; i++) {
      addrs[i] = (InetAddress) v.elementAt(i);
    }
    return addrs;
  }//getLocalAddresses

}//class NetworkUtility
