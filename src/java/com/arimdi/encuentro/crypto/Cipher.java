/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Derived from Cryptix Implementation: 
 * Copyright (c) 1995-2005 The Cryptix Foundation Limited.
 * All rights reserved.
 ***/
package com.arimdi.encuentro.crypto;

import com.arimdi.encuentro.Encuentro;

/**
 * Provides AES CBC ciphering with padding of data blocks.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Cipher {

  private Object m_SessionKey = new Object();

  public Cipher(String key) {
    setKey(key);
  }//Cipher

  public void setKey(String key) {
    if (key == null || key.length() == 0) {
      throw new RuntimeException("No key was specified.");
    } else {
      byte[] keybt = null;
      try {
        keybt = MD5.digest(key.getBytes("UTF-8"));
      } catch (Exception ex) {
        throw new RuntimeException("Could not generate key digest.");
      }
      try {
        synchronized (m_SessionKey) {
          m_SessionKey = AES.makeKey(keybt);
        }
      } catch (Exception ex) {
        ex.printStackTrace();
        throw new RuntimeException("Could not generate session key.");
      }
    }
  }//setKey

  /**
   * Encrypts some data using CBC mode and padding as described
   * in <a href="">http://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Padding</a>.
   * <p/>
   *
   * @param data the data buffer.
   * @param payload the payload of the buffer.
   * @return the new payload.
   */
  public final int encrypt(byte[] data, int payload) {
    int iv = Encuentro.getRandomInt();
    byte[] ivbytes = Encuentro.toBytes(iv);
    //System.out.println("IV bytes= " + toString(ivbytes,4));
    byte[] ivb = makeIV(ivbytes);  //previous cipher block, init with iv

    int blockcount = payload / 16;
    //padding should be 1-16 bytes
    int padding = 16 - (payload - (16 * blockcount));
    int newpayload = payload + padding;
    //pad
    byte pb = (byte) padding;
    for(int i = payload; i<newpayload;i++) {
      data[i] = pb;
    }
    //1. Encrypt first block using IV to start CBC
    inSituXOR(data, ivb, 0);
    AES.inSituEncrypt(data, 0, m_SessionKey);

    //2. Encrypt rest of blocks
    for (int i = 16; i < newpayload; i += 16) {
      //1. XOR with previous cipher block
      inSituXOR(data, i);
      AES.inSituEncrypt(data, i,m_SessionKey);
    }

    //prefix with iv bytes (4 = int)
    System.arraycopy(data,0,data,4,newpayload);
    System.arraycopy(ivbytes,0,data,0,4);

    return newpayload+4;
  }//encrypt

  /**
   * Decrypts data encrypted with {@link #encrypt(byte[],int)}.
   * <p/>
   *
   * @param data the data buffer.
   * @param payload the payload of the buffer.
   * @return the actual payload without padding.
   */
  public final int decrypt(byte[] data, int payload) {

    byte[] ivbytes = new byte[4];
    System.arraycopy(data,0,ivbytes,0,4);
    byte[] ivb = makeIV(ivbytes);  //previous cipher block, init with iv
    byte[] pcb = new byte[16];

    //copy first block encrypted for next cbc xor
    System.arraycopy(data,4,pcb,0,16);
    //1. decrypt first block using IV
    AES.inSituDecrypt(data, 4,m_SessionKey);
    inSituXOR(data, ivb, 4);

    //2. decrypt following blocks
    for (int i = 20; i < payload; i += 16) {
      //store ciphertext in ivb
      System.arraycopy(data,i,ivb,0,16);
      AES.inSituDecrypt(data, i, m_SessionKey);
      inSituXOR(data,pcb, i);
      //copy into previous
      System.arraycopy(ivb,0,pcb,0,16);
    }
    //real payload is without padding and without iv
    int newpayload =  payload - data[payload-1] - 4;
    System.arraycopy(data,4,data,0,newpayload);
    return newpayload;
  }//decrypt

  private final void inSituXOR(byte[] bl1, byte[] bl2,int offset) {
    int n = 0;
    for (int i = offset; i < offset + 16; i++) {
      bl1[i] = (byte) (bl1[i] ^ bl2[n++]);
    }
  }//inSituXOR

  private final void inSituXOR(byte[] bl, int offset) {
    int n = offset - 16;

    for (int i = offset; i < offset + 16; i++) {
      bl[i] = (byte) (bl[i] ^ bl[n]);
      n++; //increment previous block offset
    }
  }//inSituXOR

  private final byte[] makeIV(byte[] iv) {
    return MD5.digest(iv);
  }//makeIV

}//class Cipher
