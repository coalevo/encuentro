/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.Encuentro;
import com.arimdi.encuentro.util.CountDownLatch;
import com.arimdi.encuentro.service.NetworkServices;
import com.arimdi.encuentro.service.NetworkServicesConfiguration;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

import java.util.Dictionary;
import java.util.Properties;

/**
 * This class implements a <tt>BundleActivator</tt> for
 * OSGi.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class Activator
    implements BundleActivator {

  private static ENode c_NetworkServices;

  public void start(final BundleContext bundleContext)
      throws Exception {

    Thread t = new Thread(
        new Runnable() {
          public void run() {
            try {
              //1. Prepare and activate service
              c_NetworkServices = (ENode) Encuentro.getNetworkServices();
              c_NetworkServices.activate();
              
              //2. Register bundle configuration
              BundleConfiguration bc = new BundleConfiguration();
              bc.activate(bundleContext);

              //3. Register service
              bundleContext.registerService(
                  NetworkServices.class.getName(),
                  c_NetworkServices,
                  null
              );

            } catch (Exception ex) {
              ex.printStackTrace();
            }
          }//run
        }//Runnable
    );//Thread
    t.setContextClassLoader(getClass().getClassLoader());
    t.start();
  }//start

  public void stop(BundleContext bundleContext) throws Exception {
    if (c_NetworkServices != null) {
      c_NetworkServices.deactivate();
    }
    c_NetworkServices = null;
  }//stop

  class BundleConfiguration
      implements NetworkServicesConfiguration {

    private CountDownLatch m_WaitLatch;

    public void activate(BundleContext bc) {
      m_WaitLatch = new CountDownLatch(1);
      final String identifier = NetworkServicesConfiguration.class.getName();
      final Properties p = new Properties();
      p.put(org.osgi.framework.Constants.SERVICE_PID, identifier);
      String[] classes = new String[]{ManagedService.class.getName()};
      bc.registerService(classes, this, p);
      //config admin
      ConfigurationAdmin ca = null;
      ServiceReference ref = bc.getServiceReference(
          ConfigurationAdmin.class.getName());
      if (ref != null) {
        ca = (ConfigurationAdmin) bc.getService(ref);
      }
      if (ca == null) {
        return;
      }
      try {
        Configuration[] c = ca.listConfigurations("(" + org.osgi.framework.Constants.SERVICE_PID + "=" + identifier + ")");
        if (c == null || c.length == 0) {
          Configuration config = ca.getConfiguration(identifier);
          //update with defaults
          config.update(getDefaults());
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
      try {
        m_WaitLatch.await();
      } catch (InterruptedException iex) {
      }
    }//activate

    public void updated(Dictionary dictionary) throws ConfigurationException {
      if(dictionary == null) {
        return;
      }
      final ClassLoader cl = Thread.currentThread().getContextClassLoader();
      Thread.currentThread().setContextClassLoader(getClass().getClassLoader());
      try {
        boolean debug =
            "true".equals(dictionary.get("com.arimdi.encuentro.debug").toString());
        boolean secure =
            "true".equals(dictionary.get("com.arimdi.encuentro.secure").toString());
        String key = dictionary.get("com.arimdi.encuentro.secure.key").toString();
        int mtu = Integer.parseInt(dictionary.get("com.arimdi.encuentro.mtu").toString());
        boolean announce =
            "true".equals(dictionary.get("com.arimdi.encuentro.announce").toString());
        
        Encuentro.reconfigure(debug, mtu, secure, key, announce);
      } finally {
        Thread.currentThread().setContextClassLoader(cl);
        if(dictionary != null) {
          m_WaitLatch.countDown();
        }
      }
    }//updated

    public Dictionary getDefaults() {
      final Properties defaults = new Properties();
          defaults.put(DEBUG, new Boolean(false));
          defaults.put(ANNOUNCE, new Boolean(false));
          defaults.put(SECURE, new Boolean(false));
          defaults.put(SECURE_KEY, new String("key475"));
          defaults.put(MTU, new Integer(1400));
      return defaults;
    }//getDefaults;

  }//BundleConfiguration

}//class Activator
