/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.Encuentro;
import com.arimdi.encuentro.model.ServiceTemplate;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Class implementing a request message.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
class Request
    extends EMessage {

  protected EServiceTemplate m_ServiceTemplate;

  public Request(EServiceTemplate st) {
    setType(TYPE_REQUEST);
    m_ServiceTemplate = st;
    setIdentifier(Encuentro.getRandomIdentifier());
  }//constructor

  public Request() {
  }//constructor

  public ServiceTemplate getServiceTemplate() {
    return m_ServiceTemplate;
  }//setTemplate

  public void setServiceTemplate(EServiceTemplate serviceTemplate) {
    m_ServiceTemplate = serviceTemplate;
  }//setTemplate

  public int getOutputLength() {
    return super.getOutputLength() + m_ServiceTemplate.getOutputLength();
  }//getOutputLength

  protected void writeData(DataOutput out)
      throws IOException {
    m_ServiceTemplate.writeTo(out);
  }//writeData

  public void readData(DataInput in)
      throws IOException {
    m_ServiceTemplate = new EServiceTemplate();
    m_ServiceTemplate.readFrom(in);
  }//readFrom

}//class Request
