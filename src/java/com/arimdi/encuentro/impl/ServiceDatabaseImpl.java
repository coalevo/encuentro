/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.Encuentro;
import com.arimdi.encuentro.model.Service;
import com.arimdi.encuentro.model.ServiceDatabase;
import com.arimdi.encuentro.model.ServiceTemplate;
import com.arimdi.encuentro.model.TransportType;

import java.util.Hashtable;
import java.util.Vector;

/**
 * A data structure for holding services.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
class ServiceDatabaseImpl implements ServiceDatabase {

  protected Vector m_Services;
  protected Hashtable m_IdentifierIndex;
  protected Hashtable m_DomainsIndex;
  protected Hashtable m_AddressIndex;
  protected Hashtable m_NameIndex;

  public ServiceDatabaseImpl() {
    m_Services = new Vector(10);
    m_IdentifierIndex = new Hashtable();
  }//ServiceDatabase

  public ServiceDatabaseImpl(Service[] services) {
    m_Services = new Vector(services.length + 10);
    m_IdentifierIndex = new Hashtable((int) (services.length * 1.3));
    for (int i = 0; i < services.length; i++) {
      addService(services[i]);
    }
  }//ServiceDatabase

  /**
   * Adds a service to this <tt>ServiceDatabase</tt>.
   *
   * @param s the {@link Service} instance to be added.
   * @return true if added, false otherwise.
   */
  public boolean addService(Service s) {
    if (!contains(s)) {
      m_Services.addElement(s);
      m_IdentifierIndex.put(s.getIdentifier(), s);
      return true;
    }
    return false;
  }//addService

  /**
   * Removes a service from this <tt>ServiceDatabase</tt>.
   *
   * @param s the {@link Service} instance to be removed.
   */
  public void removeService(Service s) {
    if (m_Services.removeElement(s)) {
      m_IdentifierIndex.remove(s.getIdentifier());
    }
  }//removeService

  public boolean contains(Service s) {
    return m_IdentifierIndex.containsKey(s.getIdentifier());
  }//contains

  public Service[] findService(ServiceTemplate st) {
    if(Encuentro.debug) {
      System.out.println("findingService()::looking for " + st.toString());
      System.out.println("findingService()::could be in " + this.toString());
    }

    Vector matched = new Vector(m_Services.size());
    for (int i = 0; i < m_Services.size(); i++) {
      Service s = (Service) m_Services.elementAt(i);
      if (s.matches(st)) {
        matched.addElement(s);
      }
    }
    EService[] match = new EService[matched.size()];
    for (int i = 0; i < match.length; i++) {
      match[i] = (EService) matched.elementAt(i);
    }
    return match;
  }//findService

  public Service[] getServices() {
    int size = size();
    EService[] all = new EService[size];
    for (int i = 0; i < size; i++) {
      all[i] = (EService) m_Services.elementAt(i);
    }
    return all;
  }//getServices

  public int size() {
    return m_Services.size();
  }//size

  /**
   * Removes all services from this <tt>ServiceDatabase</tt>.
   */
  public void clear() {
    m_Services.removeAllElements();
  }//clear

  public String toString() {
    StringBuffer sbuf = new StringBuffer();
    int size = size();
    for (int i = 0; i < size; i++) {
      sbuf.append(((Service) m_Services.elementAt(i)).toString());
      if (i < size) {
        sbuf.append(System.getProperty("line.separator"));
      }
    }
    return sbuf.toString();
  }//toString

  public Hashtable createTreeIndex() {
    Service[] services = getServices();
    Hashtable domains = new Hashtable();

    m_DomainsIndex = new Hashtable((int) (services.length * 1.3));
    m_AddressIndex = new Hashtable((int) (services.length * 1.3));
    m_NameIndex = new Hashtable((int) (services.length * 1.3));
    for (int i = 0; i < services.length; i++) {
      Service s = (Service) services[i];
      //if (Encuentro.debug) System.out.println(s.toIdentityString());
      String domain = s.getDomain();
      Hashtable addresses = null;
      if (domains.containsKey(domain)) {
        addresses = (Hashtable) domains.get(domain);
      } else {
        addresses = new Hashtable();
        domains.put(domain, addresses);
      }
      String address = s.getAddress().getHostAddress();
      Hashtable types = null;
      if (addresses.containsKey(address)) {
        types = (Hashtable) addresses.get(address);
      } else {
        types = new Hashtable();
        addresses.put(address, types);
      }
      String type = s.getType();
      Hashtable ttypes = null;
      if (types.containsKey(type)) {
        ttypes = (Hashtable) types.get(type);
      } else {
        ttypes = new Hashtable();
        types.put(type, ttypes);
      }
      TransportType ttype = s.getTransportType();
      Hashtable srvs = null;
      if (ttypes.containsKey(ttype)) {
        srvs = (Hashtable) ttypes.get(ttype);
      } else {
        srvs = new Hashtable();
        ttypes.put(ttype, srvs);
        //System.out.println("MUST BE TRUE->"+ttypes.contains(ttype));
      }
      srvs.put(s, "");
    }
    if (Encuentro.debug) System.out.println(domains.toString());
    return domains;
  }//createTreeIndex

}//class ServiceDatabase
