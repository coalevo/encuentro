/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.Encuentro;
import com.arimdi.encuentro.crypto.Cipher;
import com.arimdi.encuentro.io.BytesInputStream;
import com.arimdi.encuentro.io.BytesOutputStream;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.MulticastSocket;

/**
 * Class implementing the <tt>Encuentro</tt> transport.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
class ETransport {

  /**
   * Streams sending and receiving messages.
   */
  private BytesOutputStream m_ByteOut;
  private BytesInputStream m_ByteIn;
  private MulticastSocket m_Socket;

  public ETransport(MulticastSocket msock) {
    m_Socket = msock;
    int mtu = Encuentro.getMTU();
    m_ByteOut = new BytesOutputStream(mtu);
    m_ByteIn = new BytesInputStream(mtu);
  }//constructor

  public void sendMessage(EMessage m)
      throws IOException {

    if (Encuentro.debug) System.out.println("ETransport::sendMessage()");
    try {
      synchronized (m_ByteOut) {
        m_ByteOut.reset();
        m.writeTo(m_ByteOut);
        byte[] buffer = m_ByteOut.getBuffer();
        int payload = m_ByteOut.size();
        if (Encuentro.secure) {
          Cipher c = Encuentro.getCipher();
          payload = c.encrypt(buffer, payload);
        }
        DatagramPacket dp = new DatagramPacket(buffer,
            payload,
            m.getAddress(),
            m.getPort());
        m_Socket.send(dp);
        //System.out.println("sendMessage()::length="+dp.getLength());
        // if (Encuentro.debug) System.out.println("Wrote  = " + MD5.asHex(m_ByteOut.getBuffer(), 0, m_ByteOut.size()));
        if (Encuentro.debug) System.out.println("ETransport::Sent message.");
      }
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new IOException(ex.getMessage());
    }
  }//sendMessage

  public EMessage receiveMessage()
      throws IOException {
    if (Encuentro.debug) System.out.println("ETransport::receiveMessage()");
    try {
      synchronized (m_ByteIn) {
        byte[] buffer = m_ByteIn.getBuffer();
        DatagramPacket dp = new DatagramPacket(buffer, m_ByteIn.getBufferLength());
        m_Socket.receive(dp);
        int pkglen = dp.getLength();
        if (pkglen < 1) {
          if (Encuentro.debug) System.out.println("ETransport::receiveMessage()::Package Length wrong = " + pkglen);
          return null;
        }
        m_ByteIn.reset(pkglen);
        if (Encuentro.secure) {
          if (Encuentro.debug) System.out.println("ETransport::receiveMessage()::Trying to decrypt");
          Encuentro.getCipher().decrypt(buffer, pkglen);
        }
        //if (Encuentro.debug) System.out.println("Read  = " + MD5.asHex(m_ByteIn.getBuffer(),0,dp.getLength()));
        if (Encuentro.debug) System.out.println("ETransport::Received message.");
        EMessage m = EMessage.createMessage(m_ByteIn);
        m.setAddress(dp.getAddress());
        m.setPort(dp.getPort());
        return m;
      }
    } catch (Exception ex) {
      if (Encuentro.debug) ex.printStackTrace();
      if (!"Receive timed out".equals(ex.getMessage()) && !Encuentro.secure) {
        //ex.printStackTrace();
        throw new IOException(ex.getMessage());
      } else {
        return null;
      }
    }
  }//receiveMessage

}//class ETransport
