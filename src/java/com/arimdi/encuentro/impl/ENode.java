/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.Encuentro;
import com.arimdi.encuentro.model.*;
import com.arimdi.encuentro.service.*;
import com.arimdi.encuentro.util.LinkedQueue;
import com.arimdi.encuentro.util.NetworkUtility;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;

/**
 * Provides an implementation of {@link NetworkServices} that
 * uses the <tt>Encuentro</tt> protocol.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public class ENode
    implements NetworkServices {

  protected MulticastSocket m_Socket;
  protected ETransport m_Transport;
  protected IncomingMessageHandler m_Handler;
  protected TransactionHandler m_TransactionHandler;
  protected IncomingMessageHandler m_MessageHandler;
  protected Responder m_Responder;
  protected LinkedQueue m_PendingRequests;
  protected ServiceDatabaseImpl m_ServiceDatabase;
  protected boolean m_Startup = true;
  private boolean m_Announcing = Encuentro.ANNOUNCE_REGISTRATION;

  public ENode() {
    m_TransactionHandler = new TransactionHandler(25);
    m_ServiceDatabase = new ServiceDatabaseImpl();
    m_PendingRequests = new LinkedQueue();
  }//constructor

  public void startup()
      throws Exception {

    //1. prepare Socket
    m_Socket = new MulticastSocket(Encuentro.PORT);
    //m_Socket.setSoTimeout(1000);
    m_Socket.setTimeToLive(1);
    //m_Socket.setReceiveBufferSize(4096);
    //m_Socket.setSendBufferSize(2048);

    m_Socket.joinGroup(Encuentro.getMulticastAddress());

    //2. prepare transport
    m_Transport = new ETransport(m_Socket);

    //3. start incoming message handler
    m_MessageHandler = new IncomingMessageHandler();
    m_MessageHandler.start();

    //4. start responder
    m_Responder = new Responder();
    m_Responder.start();
    m_Startup = false;
  }//startup

  public void shutdown()
      throws Exception {
    if(Encuentro.debug) {
      System.out.println("ENode::shutdown()");
    }
    m_Responder.stop();
    m_MessageHandler.stop();
    m_Socket.leaveGroup(Encuentro.getMulticastAddress());

    if(Encuentro.debug) {
      System.out.println("ENode::shutdown()::left group");
    }
    try {
      m_Socket.close();
    } catch (Exception ex) {

    }
    if(Encuentro.debug) {
      System.out.println("ENode::shutdown()::socket closed");
    }
  }//shutdown

  public void unregisterLocalService(Service s) {
    try {
      if (m_Announcing) {
        ServiceDown msg = new ServiceDown((EService)s);
        msg.setAddress(Encuentro.getMulticastAddress());
        msg.setPort(Encuentro.PORT);
        msg.setOriginalNode(true);
        m_Transport.sendMessage(msg);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    } finally {
      //remove from database
      m_ServiceDatabase.removeService(s);
    }
  }//deregisterLocalService

  protected boolean registerLocalService(Service s) {
    if (m_ServiceDatabase.contains(s)) {
      return false;
    }
    try {
      if (m_Announcing) {
        //send announcement
        ServiceUp msg = new ServiceUp((EService)s);
        msg.setAddress(Encuentro.getMulticastAddress());
        msg.setPort(Encuentro.PORT);
        msg.setOriginalNode(true);
        m_Transport.sendMessage(msg);
      }
      //add to database
      return m_ServiceDatabase.addService(s);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return true;
  }//registerLocalService

  protected Service[] discoverService(EServiceTemplate st, int timeout, boolean single) {
    try {
      //send discovery message
      Request req = new Request(st);
      req.setOriginalNode(false);
      req.setPort(Encuentro.PORT);
      req.setAddress(Encuentro.getMulticastAddress());
      req.setTimeout(timeout);
      TransactionHandler.Transaction t =
          m_TransactionHandler.prepareTransaction(req, single);
      m_Transport.sendMessage(req);
      EService[] sr = m_TransactionHandler.execute(t, req.getTimeout());
      if (sr != null && sr.length > 0) {
        //extra check, all answers must match the template
        for (int i = 0; i < sr.length; i++) {
          if (!sr[i].matches(st)) {
            return NO_SERVICES;
          }
        }
        return sr;
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    //no response in time
    return NO_SERVICES;
  }//discoverService

  //***** Network Services Interface implementation  *********************//

  public void activate()
      throws Exception {
    if (m_Startup) {
      this.startup();
    }
  }//activate

  public void deactivate()
      throws Exception {
    this.shutdown();
  }//deactivate

  public Service[] query(ServiceTemplate st, InetAddress srv) {
    return new Service[0];
  }

  public Service discover(ServiceTemplate st) {
    return discoverService((EServiceTemplate)st, -1, true)[0];
  }//discover

  public Service discover(ServiceTemplate st, int time) {
    return discoverService((EServiceTemplate)st, time, true)[0];
  }//discover

  public Service[] discoverAll(ServiceTemplate st, int time) {
    return discoverService((EServiceTemplate)st, time, false);
  }//discoverAll

  public boolean register(Service s)
      throws ServiceConflict {
    if (m_ServiceDatabase.contains(s)) {
      throw new ServiceConflict("Service duplicate.");
    }
    return registerLocalService(s);
  }//registerLocalService

  public void unregister(Service s) {
    unregisterLocalService(s);
  }//unregister

  public void unregisterAll() {
    Service[] ls = m_ServiceDatabase.getServices();
    for (int i = 0; i < ls.length; i++) {
      unregister(ls[i]);
    }
  }//unregisterAll

  public boolean isAnnouncing() {
    return m_Announcing;
  }//isAnnouncingRegistrations

  public void setAnnouncing(boolean b) {
    m_Announcing = b;
  }//setAnnouncingRegistrations

  public ServiceTemplate createServiceTemplate() {
    return new EServiceTemplate();
  }//createServiceTemplate

  public Service createService() {
    return new EService();
  }//createService

  public InetAddress[] getLocalAddresses() throws Exception {
    return NetworkUtility.getLocalAddresses();
  }//getLocalAddresses

  public InetAddress getLinkLocalHost() throws Exception {
    return NetworkUtility.getLinkLocalHost();
  }//getLocalAddresses

  public InetAddress[] getIP4LLAddresses() throws Exception {
    return NetworkUtility.getIP4LLAddresses();
  }//getIP4LLAddresses

  //*** Incoming Message handler **//
  class IncomingMessageHandler
      implements Runnable {

    protected boolean m_Active = true;
    protected Thread m_Thread;
    protected boolean m_Running = false;

    public void start() {
      if (!m_Running) {
        m_Thread = new Thread(this);
        m_Thread.start();
      }
    }//start

    public void stop() {
      m_Active = false;
      m_Thread.interrupt();
      if(Encuentro.debug) {
        System.out.println("ENode::IncomingMessageHandler::stop()");
      }
    }//stop

    public void run() {
      m_Running = true;
      while (m_Active) {
        try {
          EMessage m = m_Transport.receiveMessage();
          if (m != null) {//received something
            if (m instanceof Request) {  //handle Request
              if (Encuentro.debug) System.out.println("Request received.");
              if (!m_Startup) {
                m_PendingRequests.put(m);
              }
            } else if (m instanceof Response) { //handle Response
              if (Encuentro.debug) System.out.println("Response received.");
              m_TransactionHandler.probableAnswer((Response) m);
            } else if (m instanceof ServiceUp) {
              if (Encuentro.debug) System.out.println("ServiceUp received.");
              m_TransactionHandler.probableAnswer((ServiceUp) m);
            } else if (m instanceof ServiceDown) {
              if (Encuentro.debug) System.out.println("ServiceDown received.");
            }
          }
        } catch (SocketException sex) {
          if(Encuentro.debug) {
            System.out.println("ENode::IncomingMessageHandler::SocketException");
          }
          if (m_Active) {
            sex.printStackTrace();
          }
        } catch (InterruptedException ex) {
          if(Encuentro.debug) {
            System.out.println("ENode::IncomingMessageHandler::InterruptedException");
          }
          if (m_Active) {
            ex.printStackTrace();
          }
          //do nothing
        } catch (IOException ex) {
          if(Encuentro.debug) {
            System.out.println("ENode::IncomingMessageHandler::IOException");
          }
          if (m_Active) {
            ex.printStackTrace();
          }
        } catch (Exception ex) {
          if(Encuentro.debug) {
            System.out.println("ENode::IncomingMessageHandler::Exception");
          }
          if(m_Active) {
            ex.printStackTrace();
          }
        }
      }
      m_Running = false;
    }//run

  }//inner class IncomingMessageHandler


  //*** Responder ***//
  class Responder
      implements Runnable {

    protected boolean m_Active = true;
    protected Thread m_Thread;
    protected boolean m_Running;

    public Responder() {
    }//constructor

    public void start() {
      if (!m_Running) {
        m_Thread = new Thread(this);
        m_Thread.start();
      }
    }//startResponder

    public void stop() {
      m_Active = false;
      m_Thread.interrupt();
      if(Encuentro.debug) {
        System.out.println("ENode::Responder::stop()");
      }
    }//stopResponder

    public void run() {
      m_Running = true;
      //System.out.println("Started Responder");
      while (m_Active || !m_PendingRequests.isEmpty()) {
        try {
          //Object o = m_PendingRequests.poll(500);
          Object o = m_PendingRequests.take();
          if (o != null) {
            Request req = (Request) o;
            //find in db
            EService[] services = (EService[]) m_ServiceDatabase.findService(req.getServiceTemplate());
            if (services.length > 0) {
              if (Response.splitRequired(services)) {
                Response[] responses = Response.createResponses(services);
                for (int i = 0; i < responses.length; i++) {
                  Response res = responses[i];
                  res.setAddress(req.getAddress()); //unicast
                  respond(req.getIdentifier(), res);
                }
              } else {
                Response r = new Response(services);
                r.setAddress(req.getAddress()); //unicast
                respond(req.getIdentifier(), r);
              }
            }
          }
        } catch (InterruptedException ex) {
          if (m_Active) {
            ex.printStackTrace();
          }
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
      m_Running = false;
    }//run

    private void respond(int reqid, Response res)
        throws Exception {
      res.setIdentifier(reqid);
      res.setAddress(Encuentro.getMulticastAddress());
      res.setPort(Encuentro.PORT);
      res.setOriginalNode(true);
      m_Transport.sendMessage(res);
    }
  }//inner class Responder

  private static final Service[] NO_SERVICES = new Service[0];

}//class ENode
