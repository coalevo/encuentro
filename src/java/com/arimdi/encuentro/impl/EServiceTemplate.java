/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.model.ServiceTemplate;
import com.arimdi.encuentro.model.TransportType;
import com.arimdi.encuentro.util.StringUtil;
import com.arimdi.encuentro.model.Transportable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * Class implementing a <tt>ServiceTemplate</tt> for Encuentro.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 * @see com.arimdi.encuentro.service.NetworkServices#discover(com.arimdi.encuentro.model.ServiceTemplate)
 * @see com.arimdi.encuentro.service.NetworkServices#discover(com.arimdi.encuentro.model.ServiceTemplate, int)
 * @see com.arimdi.encuentro.service.NetworkServices#discoverAll(com.arimdi.encuentro.model.ServiceTemplate, int)
 */
class EServiceTemplate
    implements Transportable, ServiceTemplate {

  protected String m_Type;
  protected String m_Domain;
  protected TransportType m_TransportType;
  protected String m_Name;
  protected String m_Query;

  /**
   * Constructs a new <tt>EServiceTemplate</tt> instance.
   * The domain defaults to <em>local.</em>, type and
   * transport type default to <em>any</em>.
   */
  public EServiceTemplate() {
    m_Domain = "local";
    m_Type = "any";
    m_TransportType = TransportType.ANY;
    m_Name = "any";
    m_Query = "";
  }//constructor

  public String getName() {
    return m_Name;
  }//getName

  public void setName(String name) {
    m_Name = name;
  }//setInstance

  public String getType() {
    return m_Type;
  }//getType

  public String getQuery() {
    return m_Query;
  }//getQuery

  public void setQuery(String q) {
    m_Query = q;
  }//setQuery

  public void setType(String type) {
    if (StringUtil.getUTFOutputLength(type) > 254) {
      throw new IllegalArgumentException("Type exceeds maximum length.");
    }
    m_Type = type;
  }//setType

  public String getDomain() {
    return m_Domain;
  }//getDomain

  public void setDomain(String domain) {
    //test null arguments
    if (domain == null || domain.length() == 0) {
      throw new IllegalArgumentException("Domain name invalid.");
    }
    //extract US-ASCII
    byte[] da = domain.getBytes();
    if (da.length > 254) {
      throw new IllegalArgumentException("Domain name exeeds maximum length.");
    }
    //Test printable US-ASCII excluding '='
    for (int i = 0; i < da.length; i++) {
      if (da[i] < 0x20 || da[i] > 0x7e || da[i] == 0x3d) {
        throw new IllegalArgumentException("Name not within printable US-ASCII or contains =.");
      }
    }
    m_Domain = domain;
  }//setDomain

  public TransportType getTransportType() {
    return m_TransportType;
  }//getTransportType

  public void setTransportType(TransportType ttype) {
    m_TransportType = ttype;
  }//getTransportType

  @Override
  public String toString() {
    return "EServiceTemplate{" +
        "m_Type='" + m_Type + '\'' +
        ", m_Domain='" + m_Domain + '\'' +
        ", m_TransportType=" + m_TransportType +
        ", m_Name='" + m_Name + '\'' +
        ", m_Query='" + m_Query + '\'' +
        '}';
  }//toString

  public int getOutputLength() {
    return (1 + StringUtil.getUTFOutputLength(m_Type) +
        StringUtil.getUTFOutputLength(m_Name) + m_Domain.length());
  }//getOutputLength

  public void writeTo(DataOutput dout)
      throws IOException {
    //domain
    dout.writeByte(m_Domain.length());
    //dout.writeBytes(getDomain());
    int len = m_Domain.length();
    for (int i = 0; i < len; i++) {
      dout.write((byte) m_Domain.charAt(i));
    }

    //transport type
    dout.writeByte(m_TransportType.getValue());
    //type
    dout.writeUTF(m_Type);
    //name
    dout.writeUTF(m_Name);
    //query
    dout.writeUTF(m_Query);
  }//writeTo

  public void readFrom(DataInput din)
      throws IOException {
    //domain
    int toread = din.readUnsignedByte();
    StringBuffer sbuf = new StringBuffer();
    while (toread > 0) {
      sbuf.append((char) din.readByte());
      toread--;
    }
    setDomain(sbuf.toString());
    //transport type
    setTransportType(TransportType.getTransportType(din.readUnsignedByte()));
    //type
    m_Type = din.readUTF();
    //name
    m_Name = din.readUTF();
    //query
    m_Query = din.readUTF();
  }//readFrom

}//class EServiceTemplate

