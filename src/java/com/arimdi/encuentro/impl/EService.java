/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.Encuentro;
import com.arimdi.encuentro.model.Service;
import com.arimdi.encuentro.model.*;
import com.arimdi.encuentro.model.TransportType;
import com.arimdi.encuentro.crypto.MD5;
import com.arimdi.encuentro.util.StringUtil;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Vector;

/**
 * Provides an implementation of {@link Service} within the context
 * of the <tt>Encuentro</tt> protocol.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
class EService
    implements Service {

  protected String m_Type;
  protected String m_Domain = "local";
  protected TransportType m_TransportType;
  protected String m_Name;
  protected InetAddress m_Address;
  protected int m_Port;
  protected int m_Weight;
  protected Vector m_Properties;
  protected int m_Priority;
  protected String m_Identifier;
  protected int m_PropertiesOutputLength;

  /**
   * Constructs a new <tt>Service</tt> instance.
   */
  public EService() {
    m_Properties = new Vector(10);
    m_Address = Encuentro.getLocalAddress();
  }//constructor

  public String getIdentifier() {
    if (m_Identifier == null) {
      m_Identifier = MD5.hash(toIdentityString());
    }
    return m_Identifier;
  }//getIdentifier

  public String getType() {
    return m_Type;
  }//getType  

  public void setType(String type) {
     if (StringUtil.getUTFOutputLength(type) > 254) {
      throw new IllegalArgumentException("Type exceeds maximum length.");
    }
    m_Type = type;
    m_Identifier = null;
  }//setType

  public String getDomain() {
    return m_Domain;
  }//getDomain

  public void setDomain(String domain) {
      //test null arguments
    if (domain == null || domain.length() == 0) {
      throw new IllegalArgumentException("Domain name invalid.");
    }
    //extract US-ASCII
    byte[] da = domain.getBytes();
    if (da.length > 254) {
      throw new IllegalArgumentException("Domain name exeeds maximum length.");
    }
    //Test printable US-ASCII excluding '='
    for (int i = 0; i < da.length; i++) {
      if (da[i] < 0x20 || da[i] > 0x7e || da[i] == 0x3d) {
        throw new IllegalArgumentException("Name not within printable US-ASCII or contains =.");
      }
    }
    m_Domain = domain;
    m_Identifier = null;
  }//setDomain

  public TransportType getTransportType() {
    return m_TransportType;
  }//getTransportType

  public void setTransportType(TransportType ttype) {
    m_TransportType = ttype;
    m_Identifier = null;
  }//setTransportType

  public String getName() {
    return m_Name;
  }//getName

  public void setName(String name) {
    m_Name = name;
    m_Identifier = null;
  }//setInstance

  public InetAddress getAddress() {
    return m_Address;
  }//getAddress

  public void setAddress(InetAddress addr) {
    m_Address = addr;
    m_Identifier = null;
  }//setAddress

  public int getPort() {
    return m_Port;
  }//getPort

  public void setPort(int number) {
    if (number > Encuentro.UNSIGNED_SHORT_MAX) {
      throw new IllegalArgumentException("Number exceeds unsigned short maximum.");
    }
    m_Port = number;
    m_Identifier = null;
  }//setPort

  public int getWeight() {
    return m_Weight;
  }//getWeight

  public void setWeight(int weight) {
    if (weight > 254) {
      throw new IllegalArgumentException("Number exceeds maximum (254).");
    }
    m_Weight = weight;
    m_Identifier = null;
  }//setWeight

  public int getPriority() {
    return m_Priority;
  }//getPriority

  public void setPriority(int priority) {
    if (priority > 254) {
      throw new IllegalArgumentException("Number exceeds maximum (254).");
    }
    m_Priority = priority;
    m_Identifier = null;
  }//setPriority

  public void addProperty(ServiceProperty prop) {
    m_Properties.addElement(prop);
    m_PropertiesOutputLength += prop.getOutputLength();
    m_Identifier = null;
  }//addProperty

  public void removeProperty(int idx) {
    ServiceProperty sp = (ServiceProperty) m_Properties.elementAt(idx);
    m_PropertiesOutputLength -= sp.getOutputLength();
    m_Properties.removeElementAt(idx);
    m_Identifier = null;
  }//removeProperty

  public void removeProperty(ServiceProperty sp) {
    if (m_Properties.removeElement(sp)) {
      m_PropertiesOutputLength -= sp.getOutputLength();
    }
    m_Identifier = null;
  }//removeProperty

  public ServiceProperty[] getProperties(String name) {
    name = name.toLowerCase();
    Vector l = new Vector(m_Properties.size());
    for (int i = 0; i < m_Properties.size(); i++) {
      ServiceProperty sp = (ServiceProperty) m_Properties.elementAt(i);
      if (sp.getName().toLowerCase().equals(name)) {
        l.addElement(sp);
      }
    }
    ServiceProperty[] props = new ServiceProperty[l.size()];
    for (int i = 0; i < l.size(); i++) {
      props[i] = (ServiceProperty) l.elementAt(i);
    }
    return props;
  }//getProperties

  public ServiceProperty[] getProperties() {
    ServiceProperty[] props = new ServiceProperty[m_Properties.size()];
    for (int i = 0; i < m_Properties.size(); i++) {
      props[i] = (ServiceProperty) m_Properties.elementAt(i);
    }
    return props;
  }//getProperties

  public ServiceProperty createProperty(String name, String value) {
    return new EServiceProperty(name, value);
  }//createProperty

  /**
   * Tests if this <tt>EService</tt> matches a given template.
   *
   * @param st a <tt>ServiceTemplate</tt> instance.
   * @return true if matches, false otherwise.
   */
  public boolean matches(ServiceTemplate st) {
     if(Encuentro.debug) {
      System.out.println("matches()::of " + st.toString());
      System.out.println("matches()::against " + this.toString());
    }
    String domain = st.getDomain().toLowerCase();
    if (domain.length() == 0 || ((!domain.equals("any")) && (!domain.equals(m_Domain)))) {
      if(Encuentro.debug) System.out.println("failed domain");
      return false;
    }
    TransportType t = st.getTransportType();
    if (!t.equals(TransportType.ANY) && !t.equals(m_TransportType)) {
      if(Encuentro.debug) System.out.println("failed transport type template-t=" + t + " service-t=" + m_TransportType);
      return false;
    }
    String type = st.getType().toLowerCase();
    if (type.length() == 0 || ((!type.equals("any")) && (!type.equals(m_Type)))) {
      if(Encuentro.debug) System.out.println("failed type");
      return false;
    }
    String name = st.getName();
    if (name.length() == 0 || ((!name.equals("any")) && (!name.equals(m_Name)))) {
      return false;
    }
    if(Encuentro.debug) System.out.println("matches()::matches!");
    //TODO: Add Properties
    return true;
  }//matches

  public String toString() {
    return "EService{" +
        "m_Type='" + m_Type + '\'' +
        ", m_Domain='" + m_Domain + '\'' +
        ", m_TransportType=" + m_TransportType +
        ", m_Name='" + m_Name + '\'' +
        ", m_Address=" + m_Address +
        ", m_Port=" + m_Port +
        ", m_Weight=" + m_Weight +
        ", m_Properties=" + m_Properties +
        ", m_Priority=" + m_Priority +
        ", m_Identifier='" + m_Identifier + '\'' +
        '}';
  }//toString

  public String toIdentityString() {
    StringBuffer sbuf = new StringBuffer(getName());
    sbuf.append('.');
    sbuf.append(getDomain());
    sbuf.append(':');
    sbuf.append(getAddress().getHostAddress());
    sbuf.append(':');
    sbuf.append(getPort());
    sbuf.append(':');
    sbuf.append(getTransportType());
    sbuf.append(':');
    sbuf.append(getType());
    return sbuf.toString();
  }//toIdentityString

  public int getOutputLength() {
    return 8 + StringUtil.getUTFOutputLength(m_Name) +
        m_Domain.length() + m_Address.getAddress().length +
        StringUtil.getUTFOutputLength(m_Type) + m_PropertiesOutputLength;
  }//getOutputLength

  public void writeTo(DataOutput dout)
      throws IOException {

    //domain
    dout.writeByte(m_Domain.length());
    int len = m_Domain.length();
    for (int i = 0; i < len; i++) {
      dout.write((byte) m_Domain.charAt(i));
    }
    //transport type
    dout.writeByte(m_TransportType.getValue());
    //type
    dout.writeUTF(m_Type);
    //name
    if (m_Name == null || m_Name.length() == 0) {
      dout.writeUTF("");
    } else {
      //writeUTF will automatically prepend length as short
      dout.writeUTF(m_Name);
    }
    //address as bytes in network order
    byte[] addr = m_Address.getAddress();
    dout.writeByte(addr.length);
    dout.write(addr, 0, addr.length);
    //port
    dout.writeShort(getPort());
    //weight
    dout.writeByte(getWeight());
    //priority
    dout.writeByte(getPriority());
    //properties
    dout.writeByte(m_Properties.size());
    for (int i = 0; i < m_Properties.size(); i++) {
      ServiceProperty sp = (ServiceProperty) m_Properties.elementAt(i);
      sp.writeTo(dout);
    }
  }//writeTo

  public void readFrom(DataInput din)
      throws IOException {

    //domain
    int toread = din.readUnsignedByte();
    StringBuffer sbuf = new StringBuffer();
    while (toread > 0) {
      sbuf.append((char) din.readByte());
      toread--;
    }
    setDomain(sbuf.toString());
    //transport type
    setTransportType(TransportType.getTransportType(din.readUnsignedByte()));
    //type
    setType(din.readUTF());
    //name
    setName(din.readUTF());
    //network address
    toread = din.readUnsignedByte();
    int[] addr = new int[toread];
    while (toread > 0) {
      addr[addr.length - toread] = din.readUnsignedByte();
      toread--;
    }
    setAddress(parseAddress(addr));
    //port
    setPort(din.readUnsignedShort());
    //weight
    setWeight(din.readUnsignedByte());
    //priority
    setPriority(din.readUnsignedByte());
    //properties
    toread = din.readUnsignedByte();
    while (toread > 0) {
      ServiceProperty sp = new EServiceProperty();
      sp.readFrom(din);
      addProperty(sp);
      toread--;
    }
  }//readFrom

  public boolean equals(Object o) {
    if (o instanceof Service) {
      return ((Service) o).getIdentifier().equals(this.getIdentifier());
    } else if (o instanceof String) {
      return this.getIdentifier().equals(o);
    }
    return false;
  }//equals

  private static InetAddress parseAddress(int[] addr) {
    StringBuffer a = new StringBuffer();
    for (int i = 0; i < addr.length - 1; i++) {
      a.append(addr[i]);
      a.append('.');
    }
    a.append(addr[addr.length - 1]);
    try {
      return InetAddress.getByName(a.toString());
    } catch (Exception ex) {
      return null;
    }
  }//parseAddress

}//class EService

