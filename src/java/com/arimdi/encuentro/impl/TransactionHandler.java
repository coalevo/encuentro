/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.Encuentro;
import com.arimdi.encuentro.model.Service;
import com.arimdi.encuentro.util.LinkedQueue;

import java.util.Vector;

/**
 * Class implementing a simple timed
 * transaction handler.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
class TransactionHandler
    extends Vector {

  private LinkedQueue m_Transactions;

  public TransactionHandler(int size) {
    super(size);
    //m_PutLock = new Object();
    m_Transactions = new LinkedQueue();
    try {
      for (int i = 0; i < size; i++) {
        m_Transactions.put(new Transaction(this));
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }//TimedTransactionVector

  public Transaction prepareTransaction(Request request, boolean single) {
    Transaction t = null;
    try {
      if (m_Transactions.isEmpty()) {
        t = new Transaction(this, single);
      } else {
        t = (Transaction) m_Transactions.take();
        t.reset(request, single);
      }
      //append element
      this.addElement(t);
      if (Encuentro.debug) System.out.println("TransactionHandler::Appended Request.");
    } catch (Exception ex) {
      ex.printStackTrace();
      //this.removeElement(request);
    }
    return t;
  }//prepareTransaction

  public EService[] execute(Transaction t, int timeout) {
    if (Encuentro.debug) System.out.println("TransactionHandler::execute()");
    try {
      /*
      m_AddingRequest = true;

        //Transaction t = null;
        synchronized (m_PutLock) {
          if (m_Transactions.isEmpty()) {
            t = new Transaction(this, single);
          } else {
            t = (Transaction) m_Transactions.take();
            t.reset(request, single);
          }
          //append element
          this.addElement(t);
          if (Encuentro.debug) System.out.println("Appended Request.");
          m_PutLock.notify();
        }
        */
      if (t.execute(timeout)) {
        EService[] s = (EService[]) t.getServices();
        m_Transactions.put(t);
        return s;
      }
      /*
      } catch (Exception ex) {
        ex.printStackTrace();
        //this.removeElement(request);
      } finally {
        m_AddingRequest = false;
      }
      */
    } catch (Exception ex) {
      ex.printStackTrace();
      //this.removeElement(request);
    }
    return null;
  }//execute

  public void probableAnswer(Response r) {
    if (Encuentro.debug) System.out.println("TransactionHandler::probableAnswer(Response)");
    /*
    synchronized (m_PutLock) {
      if (m_AddingRequest) {
        try {
          if (Encuentro.debug) System.out.println("Wait putlock.");
          m_PutLock.wait();
        } catch (InterruptedException ex) {
        }
      }
     */
    if (this.isEmpty()) {
      if (Encuentro.debug) System.out.println("TransactionHandler::probableAnswer(Response)::no pending reqeuest");
      return;
    }
    Transaction t = null;
    for (int i = 0; i < size(); i++) {
      //System.out.println("i=" + i);
      t = (Transaction) elementAt(i);
      if (t.matchesRequest(r)) {
        t.addResponse(r.getServices());
      }
    }

  }//findPendingRequest

  public boolean probableAnswer(ServiceUp sup) {
    boolean ret = false;
    if (Encuentro.debug) System.out.println("TransactionHandler::probableAnswer(ServiceUp)");
    /*
    synchronized (m_PutLock) {
      if (m_AddingRequest) {
        try {
          if (Encuentro.debug) System.out.println("Wait putlock.");
          m_PutLock.wait();
        } catch (InterruptedException ex) {

        }
      }
      */
    if (this.isEmpty()) {
      if (Encuentro.debug) System.out.println("TransactionHandler::probableAnswer(ServiceUp)::no pending reqeuest");
      return false;
    } else {
      Service s = sup.getService();
      //System.out.println(s.toString());
      Transaction t = null;

      for (int i = 0; i < size(); i++) {
        t = (Transaction) elementAt(i);
        if (t.matches(s)) {
          ret = t.addResponse(sup.getService());
        }
      }
    }
    //}
    return ret;
  }//probableAnswer


  public class Transaction {

    private Vector m_Pending;
    private Request m_Request;
    private ServiceDatabaseImpl m_Services;
    private Object m_SingleLock;
    private boolean m_Single;
    private boolean m_TimedOut;
    private boolean m_Success;

    public Transaction(Vector v) {
      m_Pending = v;
      m_Services = new ServiceDatabaseImpl();
      m_SingleLock = new Object();
      m_Single = true;
    }//Transaction

    public Transaction(Vector v, boolean single) {
      m_Pending = v;
      m_Services = new ServiceDatabaseImpl();
      m_SingleLock = new Object();
      m_Single = single;
    }//Transaction

    public boolean execute(int millis)
        throws InterruptedException {

      //if (Encuentro.debug) System.out.println("Added pending request.");
      //wait until timeout (all) or notified (single)
      m_TimedOut = false;
      m_Success = false;
      synchronized (m_SingleLock) {
        if (millis == -1) {
          m_Single = true;  //to ensure nothing gets stuck
          m_SingleLock.wait();
        } else {
          //long start = System.currentTimeMillis();
          m_SingleLock.wait(millis);
          //System.out.println((System.currentTimeMillis()-start-millis));
        }
        //finished waiting or notified
        m_TimedOut = true;
        m_Success = (m_Services.size() > 0);
        m_Pending.removeElement(this);
        if (Encuentro.debug) System.out.println("TransactionHandler::Removed request.");
        m_SingleLock.notify();
      }
      return m_Success;
    }//execute

    public boolean addResponse(Service[] s) {
      if (m_TimedOut) {
        return false;
      } else if (m_Single) { // single, notify if before timeout
        try {
          //add the response
          for (int i = 0; i < s.length; i++) {
            m_Services.addService(s[i]);
          }
          synchronized (m_SingleLock) {
            m_SingleLock.notify();
            m_SingleLock.wait();
          }
        } catch (InterruptedException ex) {
        }
      } else {  //multiple add the services
        for (int i = 0; i < s.length; i++) {
          m_Services.addService(s[i]);
        }
      }
      return m_Success;
    }//addResponse

    public boolean addResponse(Service s) {
      if (m_TimedOut) {
        return false;
      } else if (m_Single) { // single, notify if before timeout
        try {
          m_Services.addService(s);
          synchronized (m_SingleLock) {
            m_SingleLock.notify();
            m_SingleLock.wait();
          }
        } catch (InterruptedException ex) {
        }
      } else {  //multiple add the services
        m_Services.addService(s);
      }
      return m_Success;
    }//addResponse

    public boolean matchesRequest(EMessage r) {
      //System.out.println("Matching request and response with identifier");
      //System.out.println("request=" + m_Request.getIdentifier());
      //System.out.println("rsponse=" + r.getIdentifier());
      return (m_Request.matchesIdentifier(r.getIdentifier()));
    }//getMessage

    public boolean matches(Service s) {
      return s.matches(m_Request.getServiceTemplate());
    }//matches

    /**
     * Returns all services in this <tt>ServiceDatabase</tt>.
     *
     * @return an array containing references to all services.
     */
    public Service[] getServices() {
      return m_Services.getServices();
    }//getServices

    public void reset(Request r, boolean single) {
      m_Request = r;
      m_Services.clear();
      m_Single = single;
    }//reset

  }//inner class Transaction

}//class TransactionHandler
