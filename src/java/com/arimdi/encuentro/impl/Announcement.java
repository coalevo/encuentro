/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.model.Service;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;


/**
 * Abstract base class for announcement messages.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
abstract class Announcement
    extends EMessage {

  protected EService m_Service;

  /**
   * Returns the service announced.
   *
   * @return a <tt>Service</tt> instance.
   */
  public Service getService() {
    return m_Service;
  }//getService

  public int getOutputLength() {
    return super.getOutputLength() + m_Service.getOutputLength();
  }//getOutputLength

  protected void writeData(DataOutput out)
      throws IOException {
    m_Service.writeTo(out);
  }//writeData

  protected void readData(DataInput in)
      throws IOException {
    m_Service = new EService();
    m_Service.readFrom(in);
  }//writeData

}//class Announcement
