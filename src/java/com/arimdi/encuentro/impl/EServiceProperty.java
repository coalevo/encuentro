/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.model.ServiceProperty;
import com.arimdi.encuentro.crypto.MD5;
import com.arimdi.encuentro.util.StringUtil;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Class encapsulating a service property
 * which is basically a <name>=<value> pair.
 * <p/>
 * The value might be omitted (which will give a boolean
 * service property), given as String or as raw data.
 * <p/>
 * The raw byte length of the whole property must not
 * exceed 254.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
class EServiceProperty
    implements ServiceProperty {

  protected String m_Name;
  protected byte[] m_Value;

  /**
   * Creates a new <tt>ServiceProperty</tt> instance with
   * a given name and value.
   *
   * @param name  the name as <tt>String</tt>.
   * @param value the value as <tt>String</tt>.
   */
  public EServiceProperty(String name, String value) {
    setName(name);
    try {
      setValue(value.getBytes("UTF-8"));
    } catch (UnsupportedEncodingException ex) {
      //should not happen, as anything in Java is utf-8
    }
    validateLength();
  }//constructor(String, String)

  /**
   * Creates a new <tt>ServiceProperty</tt> instance with
   * a given name and raw value.
   *
   * @param name  the name as <tt>String</tt>.
   * @param value the value as <tt>byte[]</tt>.
   */
  public EServiceProperty(String name, byte[] value) {
    setName(name);
    setValue(value);
    validateLength();
  }//constructor(String, byte[])

  /**
   * Creates a new boolean <tt>ServiceProperty</tt> instance
   * with a given name.
   *
   * @param name the name as <tt>String</tt>.
   */
  public EServiceProperty(String name) {
    setName(name);
    validateLength();
  }//constructor(String)

  EServiceProperty() {
  }//constructor

  public String getName() {
    return m_Name;
  }//getName

  public String getValue() {
    return new String(m_Value);
  }//getValue

  public byte[] getRawValue() {
    return m_Value;
  }//getRawValue

  public int getOutputLength() {
    return StringUtil.getUTFOutputLength(m_Name) +
        ((isBoolean()) ? 0 : (m_Value.length));
  }//getOutputLength

  public boolean isBoolean() {
    return (m_Value == null);
  }//isBoolean

  public String toString() {
    StringBuffer sbuf = new StringBuffer(m_Name);
    if (!isBoolean()) {
      sbuf.append("=");
      sbuf.append(MD5.asHex(m_Value));
    }
    return sbuf.toString();
  }//toString

  /**
   * Validates and sets the name of this <tt>ServiceProperty</tt>.
   *
   * @param name the name as <tt>String</tt>.
   * @throws IllegalArgumentException if the name is not valid.
   */
  protected void setName(String name)
      throws IllegalArgumentException {
    //test null arguments
    if (name == null || name.length() == 0
        || name.indexOf("=") > 0
        || StringUtil.getUTFOutputLength(name) > MAX_NAME_LENGTH) {
      throw new IllegalArgumentException("Name invalid.");
    }
    m_Name = name;
  }//setName

  /**
   * Sets the value of this <tt>ServiceProperty</tt>.
   *
   * @param value
   */
  protected void setValue(byte[] value) {
    m_Value = value;
  }//setValue

  /**
   * Validates the length of this <tt>ServiceProperty</tt>.
   */
  protected void validateLength() {
    if (StringUtil.getUTFOutputLength(m_Name) +
        ((isBoolean()) ? 0 : (1 + m_Value.length)) > 254
    ) {
      throw new IllegalArgumentException("Property maximum length exceeded.");
    }
  }//validateLength

  public void readFrom(DataInput din)
      throws IOException {
    //name
    m_Name = din.readUTF();
    //value
    int toread = din.readUnsignedByte();
    m_Value = new byte[toread];
    din.readFully(m_Value, 0, toread);
  }//readFrom

  public void writeTo(DataOutput dout)
      throws IOException {

    dout.writeUTF(m_Name);
    if (!isBoolean()) {
      dout.writeByte(m_Value.length);
      dout.write(m_Value, 0, m_Value.length);
    } else {
      dout.writeByte(0);
    }
  }//writeTo

}//class EServiceProperty

