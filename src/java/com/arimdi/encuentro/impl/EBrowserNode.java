/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.Encuentro;
import com.arimdi.encuentro.model.NetworkServicesBrowser;
import com.arimdi.encuentro.model.ServiceDatabase;
import com.arimdi.encuentro.model.*;
import com.arimdi.encuentro.util.LinkedQueue;
import com.arimdi.encuentro.model.Observable;
import com.arimdi.encuentro.model.Observer;

import java.net.MulticastSocket;
import java.net.SocketException;

/**
 * Provides an implementation of {@link NetworkServicesBrowser} that uses
 * the <tt>Encuentro</tt> protocol.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */

public class EBrowserNode
    extends NetworkServicesBrowser {

  protected MulticastSocket m_Socket;
  protected ETransport m_Transport;
  protected ServiceDatabaseImpl m_ServiceDatabase;
  protected int m_Timeout = 5000;
  protected int m_Refresh = 60000;
  protected boolean m_Refreshing = false;

  protected IncomingMessageHandler m_Handler;
  protected TransactionHandler m_TransactionHandler;
  protected IncomingMessageHandler m_MessageHandler;
  protected Refresher m_Refresher;
  protected LinkedQueue m_PendingRequests;

  protected boolean m_Startup = false;

  public EBrowserNode() {
    ANY_SERVICE.setDomain("any");
    m_TransactionHandler = new TransactionHandler(25);
    m_ServiceDatabase = new ServiceDatabaseImpl();
    m_PendingRequests = new LinkedQueue();
  }//constructor

  public ServiceDatabase getServiceDatabase() {
    return m_ServiceDatabase;
  }//getServiceDatabase

  public int getDiscoveryTimeout() {
    return m_Timeout;
  }//getDiscoveryTimeout

  public void setDiscoveryTimeout(int timeout) {
    m_Timeout = timeout;
  }//setDiscoveryTimeout

  public void startup()
      throws Exception {
    m_Startup = true;
    if (Encuentro.debug) System.out.println("EBrowserNode::startup()");
    //1. prepare Socket
    m_Socket = new MulticastSocket(Encuentro.PORT);
    //m_Socket.setSoTimeout(1000);
    m_Socket.setTimeToLive(1);
    //m_Socket.setReceiveBufferSize(4096);
    //m_Socket.setSendBufferSize(2048);

    m_Socket.joinGroup(Encuentro.getMulticastAddress());

    //2. prepare transport
    m_Transport = new ETransport(m_Socket);

    //3. start incoming message handler
    m_MessageHandler = new IncomingMessageHandler();
    m_MessageHandler.start();

    //4. prepare the refresher
    m_Refresher = new Refresher();
    m_Startup = false;
    refresh();
  }//startup

  public void shutdown()
      throws Exception {

    if (Encuentro.debug) System.out.println("EBrowserNode::shutdown()");
    m_MessageHandler.stop();
    try {
      m_Socket.leaveGroup(Encuentro.getMulticastAddress());
    } catch (Exception ex) {
      if (Encuentro.debug) System.out.println("EBrowserNode::shutdown()::" + ex.getMessage());
    }
    m_Socket.close();
  }//shutdown


  private void refreshServiceDatabase() {
    m_ServiceDatabase = new ServiceDatabaseImpl(
        discoverServices(ANY_SERVICE, m_Timeout, false)
    );
    notifyObservers(m_ServiceDatabase);
  }//refreshServiceDatabase


  protected Service[] discoverServices(EServiceTemplate st, int timeout, boolean single) {
    try {
      //send discovery message
      Request req = new Request(st);
      req.setOriginalNode(false);
      req.setPort(Encuentro.PORT);
      req.setAddress(Encuentro.getMulticastAddress());
      req.setTimeout(timeout);
      TransactionHandler.Transaction t =
          m_TransactionHandler.prepareTransaction(req, single);
      m_Transport.sendMessage(req);
      Service[] sr = m_TransactionHandler.execute(t, req.getTimeout());
      if (sr != null && sr.length > 0) {
        //extra check, all answers must match the template
        for (int i = 0; i < sr.length; i++) {
          if (!sr[i].matches(st)) {
            return NO_SERVICES;
          }
        }
        return sr;
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    //}
    //no response in time
    return NO_SERVICES;
  }//discoverService


  /**
   * * Network Services Browser Interface implementation  *********************
   */

  public void initialize()
      throws Exception {
    if (!m_Startup) {
      this.startup();
    }
  }//activate

  public void terminate()
      throws Exception {
    this.shutdown();
  }//deactivate

  public void refresh() {
    this.refreshServiceDatabase();
  }//refresh

  public boolean setPeriodicallyRefreshing(boolean b, int time) {
    if (b) {
      if (!m_Refreshing) {
        m_Refresh = time;
        m_Refresher.start();
        m_Refreshing = true;
      }
    } else {
      if (m_Refreshing) {
        m_Refresher.stop();
        m_Refreshing = false;
      }
    }
    return m_Refreshing;
  }//setPeriodicRefreshing

  public boolean isPeriodicallyRefreshing() {
    return m_Refreshing;
  }//isPeriodicallyRefreshing


  /**
   * * Incoming Message handler **********************************************
   */
  class IncomingMessageHandler
      implements Runnable {

    protected boolean m_Active = true;
    protected Thread m_Thread;
    protected boolean m_Running = false;

    public void start() {
      if (!m_Running) {
        m_Thread = new Thread(this);
        //m_Thread.setPriority(Thread.currentThread().getPriority() - 1);
        m_Thread.start();
      }
    }//start

    public void stop() {
      try {
        m_Active = false;
        //m_Thread.interrupt();
      } catch (Exception ex) {

      }
    }//stop

    public void run() {
      m_Running = true;
      while (m_Active) {
        try {
          EMessage m = m_Transport.receiveMessage();
          if (m != null) {//received something
            if (m instanceof Request) {  //handle Request
              if (Encuentro.debug) System.out.println("Request received and ignored.");
              //ignore
            } else if (m instanceof Response) { //handle Response
              if (Encuentro.debug) System.out.println("Response received.");
              //check for the case we are refreshing the complete database
              m_TransactionHandler.probableAnswer((Response) m);
              continue;
            } else if (m instanceof ServiceUp) {
              if (Encuentro.debug) System.out.println("ServiceUp received.");
              //add to the service database
              m_ServiceDatabase.addService(((ServiceUp) m).getService());
              notifyObservers(m_ServiceDatabase);
              continue;
            } else if (m instanceof ServiceDown) {
              if (Encuentro.debug) System.out.println("ServiceDown received.");
              //remove service from database
              m_ServiceDatabase.removeService(((ServiceDown) m).getService());
              notifyObservers(m_ServiceDatabase);
            }
          }
        } catch (SocketException sex) {
          if (m_Active) {
            sex.printStackTrace();
          }
        } catch (Exception ex) {
          ex.printStackTrace();
          m_Active = false;
        }
      }
      m_Running = false;
      if (Encuentro.debug) System.out.println("Message Handling ran out.");
    }//run

  }//inner class IncomingMessageHandler


  /**
   * ** Refresher ***************************************************
   */
  class Refresher
      implements Runnable {

    protected boolean m_Active = true;
    protected Thread m_Thread;
    protected boolean m_Running;

    public Refresher() {
    }//constructor

    public void start() {
      if (!m_Running) {
        m_Thread = new Thread(this);
        //m_Thread.setPriority(Thread.currentThread().getPriority()-1);
        m_Thread.start();
      }
    }//startResponder

    public void stop() {
      m_Active = false;
    }//stopResponder

    public void run() {
      m_Running = true;
      if (Encuentro.debug) System.out.println("Started Refresher");
      while (m_Active || !m_PendingRequests.isEmpty()) {
        try {
          refreshServiceDatabase();
          Thread.sleep(m_Refresh);
        } catch (InterruptedException ex) {
          if (m_Active) {
            ex.printStackTrace();
          }
        } catch (Exception ex) {
          ex.printStackTrace();
        }
      }
      m_Running = false;
    }//run

  }//inner class Refresher

  public static void main(String[] args) {
    try {
      EBrowserNode node = new EBrowserNode();
      Observer ob = new Observer() {
        public void update(Observable o, Object arg) {
          System.out.println("update!");
          System.out.println(arg.toString());
        }
      };
      node.addObserver(ob);
      System.out.println(node.getObserverCount());
      node.notifyObservers(null);
      node.startup();
      node.setPeriodicallyRefreshing(true, 5000);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  private static final EService[] NO_SERVICES = new EService[0];
  private static final EServiceTemplate ANY_SERVICE = new EServiceTemplate();

}//class EBrowserNode
