/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.Encuentro;
import com.arimdi.encuentro.model.Service;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Vector;

/**
 * Class implementing a response message.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
class Response
    extends EMessage {

  protected EService[] m_Services;

  public Response(EService[] s) {
    setType(TYPE_RESPONSE);
    m_Services = s;
  }//constructor

  public Response() {
  }//Response

  public Service[] getServices() {
    return m_Services;
  }//getServices

  protected void setServices(EService[] services) {
    m_Services = services;
  }//setServices

  public int getOutputLength() {
    int ol = super.getOutputLength();
    for (int i = 0; i < m_Services.length; i++) {
      ol += m_Services[i].getOutputLength();
    }
    return ol;
  }//getOutputLength

  protected void writeData(DataOutput out)
      throws IOException {
    //if(this.getOutputLength() > m_MTU) {
    //  throw new IOException("Package length exceeded.");
    //}
    out.writeByte(m_Services.length);
    for (int i = 0; i < m_Services.length; i++) {
      m_Services[i].writeTo(out);
    }
  }//writeData

  protected void readData(DataInput in)
      throws IOException {
    int answers = in.readUnsignedByte();
    m_Services = new EService[answers];
    for (int i = 0; i < answers; i++) {
      EService s = new EService();
      s.readFrom(in);
      m_Services[i] = s;
    }
  }//readFrom

  public static boolean splitRequired(EService[] services) {
    int ol = 5;
    for (int i = 0; i < services.length; i++) {
      ol += services[i].getOutputLength();
    }
    return (ol > Encuentro.getMaxPayload());
  }//splitRequired

  public static Response[] createResponses(EService[] services) {
    if (Encuentro.debug) System.out.println("creating splitted responses.");

    Vector responses = new Vector();
    int payload = Encuentro.getMaxPayload();

    Vector v = new Vector(services.length);
    int olength = 5; //response header length
    for (int i = 0; i < services.length; i++) {
      EService service = services[i];
      //System.out.println("Length= " + olength);
      olength += service.getOutputLength();
      //System.out.println("Length will be= " + olength);
      if (olength < payload) {
        v.addElement(service);
      } else {
        //System.out.println("Creating response.");
        responses.addElement(createResponse(v));
        olength = 5 + service.getOutputLength();
        v.removeAllElements();
        v.addElement(service);
      }
    }
    responses.addElement(createResponse(v));
    Response[] res = new Response[responses.size()];
    int size = res.length;
    for (int i = 0; i < size; i++) {
      res[i] = (Response) responses.elementAt(i);
      if (i != size - 1) {
        res[i].setHasFollowUp(true);
      }
    }
    return res;
  }//createResponses

  private static Response createResponse(Vector v) {
    EService[] s = new EService[v.size()];
    int size = v.size();
    for (int i = 0; i < size; i++) {
      s[i] = (EService) v.elementAt(i);
      //System.out.println("Appending "+s[i].toIdentityString());
    }
    //Response r = new Response(s);
    //System.out.println("Response length=" + r.getOutputLength());
    return new Response(s);
  }//createResponse

}//class Response
