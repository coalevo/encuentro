/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.impl;

import com.arimdi.encuentro.io.BytesInputStream;
import com.arimdi.encuentro.model.Transportable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.net.InetAddress;

/**
 * Abstract base class for all <b>Encuentro</b> messages.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
abstract class EMessage
    implements Transportable {

  private int m_Identifier;        //message identifier
  private int m_Type;              //message type
  private boolean m_OriginalNode;  //original node flag
  private boolean m_HasFollowUp;      //follow up flag for response messages

  protected InetAddress m_Address;
  protected int m_Port;
  protected int m_Timeout;

  protected EMessage() {
  }//constructor

  public int getIdentifier() {
    return m_Identifier;
  }//getID

  public void setIdentifier(int id) {
    m_Identifier = id;
  }//setID

  public int getType() {
    return m_Type;
  }//getType

  public boolean isType(int type) {
    return (type == m_Type);
  }//isType

  public void setType(int type) {
    m_Type = type;
  }//isType

  public boolean isOriginalNode() {
    return m_OriginalNode;
  }//isDirectory

  public void setOriginalNode(boolean b) {
    m_OriginalNode = b;
  }//setDirectory

  public boolean isHasFollowUp() {
    return m_HasFollowUp;
  }//isHasFollowUp

  public void setHasFollowUp(boolean followUp) {
    m_HasFollowUp = followUp;
  }//setHasFollowUp

  public InetAddress getAddress() {
    return m_Address;
  }//getAddress

  public void setAddress(InetAddress address) {
    m_Address = address;
  }//setAddress

  public int getPort() {
    return m_Port;
  }//getPort

  public void setPort(int port) {
    m_Port = port;
  }//setPort

  public int getTimeout() {
    return m_Timeout;
  }//getDiscoveryTimeout

  public void setTimeout(int timeout) {
    m_Timeout = timeout;
  }//setDiscoveryTimeout

  public void writeTo(DataOutput out)
      throws IOException {
    //type
    out.writeByte(m_Type);
    //flags
    out.writeBoolean(m_OriginalNode);
    out.writeBoolean(m_HasFollowUp);
    //identifier
    out.writeShort(m_Identifier);
    writeData(out);
  }//writeTo

  public void readFrom(DataInput in)
      throws IOException {
    //flags
    m_OriginalNode = in.readBoolean();
    m_HasFollowUp = in.readBoolean();
    //id
    m_Identifier = in.readShort();
    readData(in);
  }//readFrom

  public int getOutputLength() {
    return 5;
  }//getOutputLength

  public boolean matchesIdentifier(int i) {
    return (m_Identifier == i);
  }//matchesIdentifier

  abstract protected void writeData(DataOutput out)
      throws IOException;

  abstract protected void readData(DataInput in)
      throws IOException;

  public static EMessage createMessage(BytesInputStream in)
      throws IOException {

    EMessage m = null;
    int type = in.readUnsignedByte();
    switch (type) {
      case TYPE_REQUEST:
        m = new Request();
        break;
      case TYPE_RESPONSE:
        m = new Response();
        break;
      case TYPE_SERVICE_UP:
        m = new ServiceUp();
        break;
      case TYPE_SERVICE_DOWN:
        m = new ServiceDown();
        break;
      default:
        throw new IOException();
    }
    m.setType(type);
    m.readFrom(in);
    return m;
  }//createMessage

  /**
   * Defines the announcement type.
   */
  private static final int TYPE_ANNOUNCEMENT = 0;

  /**
   * Defines the <tt>SERVICE UP</tt> announcement.
   */
  public static final int TYPE_SERVICE_UP = TYPE_ANNOUNCEMENT + 1;

  /**
   * Defines the <tt>SERVICE DOWN</tt> announcement.
   */
  public static final int TYPE_SERVICE_DOWN = TYPE_ANNOUNCEMENT + 2;

  /**
   * Defines the request type.
   */
  public static final int TYPE_REQUEST = 10;

  /**
   * Defines the response type.
   */
  public static final int TYPE_RESPONSE = 20;

}//class Message
