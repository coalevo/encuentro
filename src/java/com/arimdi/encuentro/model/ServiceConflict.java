/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.model;

/**
 * Thrown when a service conflict has occurred.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public class ServiceConflict
    extends Throwable {

  /**
   * Constructs a new <tt>ServiceConflict</tt> instance.
   *
   * @param msg a message describing the conflict as <tt>String</tt>.
   */
  public ServiceConflict(String msg) {
    super(msg);
  }//constructor

}//class ServiceConflict
