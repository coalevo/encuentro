/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.model;

import java.net.InetAddress;

/**
 * This interface defines a service.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public interface Service {

  /**
   * Returns the name of this <tt>ServiceTemplate</tt> instance.
   *
   * @return the name as <tt>String</tt>.
   */
  public String getName();

  /**
   * Sets the name of this <tt>ServiceTemplate</tt> instance.
   *
   * @param name the name as <tt>String</tt>.
   */
  public void setName(String name);

  /**
   * Returns the type of this <tt>ServiceTemplate</tt>.
   *
   * @return the type as <tt>String</tt>.
   */
  public String getType();

  /**
   * Sets the type of this <tt>ServiceTemplate</tt>.
   *
   * @param type the type as <tt>String</tt>.
   */
  public void setType(String type);

  /**
   * Returns the domain of this <tt>ServiceTemplate</tt>.
   *
   * @return the domain as <tt>String</tt>.
   */
  public String getDomain();

  /**
   * Sets the domain of this <tt>ServiceTemplate</tt>.
   *
   * @param domain the domain as <tt>String</tt>.
   */
  public void setDomain(String domain);

  /**
   * Returns the transport type of this <tt>ServiceTemplate</tt>.
   *
   * @return the transport type.
   */
  public TransportType getTransportType();

  /**
   * Sets the transport type of this <tt>ServiceTemplate</tt>.
   *
   * @param ttype the transport type.
   */
  public void setTransportType(TransportType ttype);
  

  /**
   * Returns the service identifier.<p>
   *
   * @return the identifier as <tt>String</tt>.
   */
  public String getIdentifier();

  /**
   * Returns the address of this <tt>Service</tt>.
   *
   * @return the address as <tt>InetAddress</tt>.
   */
  public InetAddress getAddress();

  /**
   * Sets the address of this <tt>Service</tt>.
   *
   * @param addr the address as <tt>InetAddress</tt>.
   */
  public void setAddress(InetAddress addr);

  /**
   * Returns the port of this <tt>Service</tt>.
   *
   * @return the port as <tt>int</tt>.
   */
  public int getPort();

  /**
   * Sets the port of this <tt>Service</tt>.
   *
   * @param number the port as <tt>int</tt>.
   */
  public void setPort(int number);

  /**
   * Returns the weight of this <tt>Service</tt>.
   *
   * @return the weight as <tt>int</tt>.
   */
  public int getWeight();

  /**
   * Sets the weight of this <tt>Service</tt>.
   *
   * @param weight the weight as <tt>int</tt>.
   */
  public void setWeight(int weight);

  /**
   * Returns the priority of this <tt>Service</tt>.
   *
   * @return the priority as <tt>int</tt>.
   */
  public int getPriority();

  /**
   * Sets the priority of this <tt>Service</tt>.
   *
   * @param priority the priority as <tt>int</tt>.
   */
  public void setPriority(int priority);

  /**
   * Adds a given propery to this <tt>Service</tt>.
   *
   * @param prop the property as <tt>ServiceProperty</tt>.
   * @see ServiceProperty
   */
  public void addProperty(ServiceProperty prop);

  /**
   * Removes the indicated property from this
   * <tt>Service</tt>.
   *
   * @param idx the index of the property to be removed as <tt>int</tt>.
   */
  public void removeProperty(int idx);

  /**
   * Removes the given property from this <tt>Service</tt>.
   *
   * @param sp the property to be removed as <tt>ServiceProperty</tt>.
   */
  public void removeProperty(ServiceProperty sp);

  /**
   * Returns an array of properties with the given name.
   *
   * @param name the property name as <tt>String</tt>.
   * @return an array of <tt>ServiceProperty</tt> instances, or
   *         an empty array if there is no property with the given
   *         name.
   */
  public ServiceProperty[] getProperties(String name);

  /**
   * Returns an array of properties of this <tt>Service</tt>.
   *
   * @return an array of <tt>ServiceProperty</tt> instances, or
   *         an empty array if this service has no property.
   */
  public ServiceProperty[] getProperties();

  /**
   * Creates and returns a {@link ServiceProperty} instance.
   *
   * @param name the name of the property.
   * @param value the value of the property, max 255 bytes.
   * @return a {@link ServiceProperty} instance.
   */
  public ServiceProperty createProperty(String name, String value);

  /**
   * Tests if this <tt>Service</tt> matches a given
   * <tt>ServiceTemplate</tt>.
   *
   * @param st a service template.
   * @return true if it matches, false otherwise.
   */
  public boolean matches(ServiceTemplate st);

}//interface Service
