/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.model;

import com.arimdi.encuentro.model.Observable;


/**
 * Abstract base class and interface for a network services
 * browser.
 * <p/>
 * The browser is supposed to handle a local service database,
 * which will be passed to observers on notification.
 * For complete database refreshes the reference to this database could
 * change.
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public abstract class NetworkServicesBrowser
    extends Observable {

  /**
   * Returns the reference to the actual service database.
   * Note that this reference could change on refreshes.
   *
   * @return the actual locally handled service database.
   */
  public abstract ServiceDatabase getServiceDatabase();

  /**
   * Gets the service discovery timeout.
   *
   * @return the timeout in milliseconds.
   */
  public abstract int getDiscoveryTimeout();

  /**
   * Sets the service discovery timeout.
   *
   * @param timeout the timeout in milliseconds.
   */
  public abstract void setDiscoveryTimeout(int timeout);

  /**
   * Initializes this <tt>NetworkServiceBrowser</tt>.
   *
   * @throws Exception if initialization fails.
   */
  public abstract void initialize() throws Exception;

  /**
   * Terminates this <tt>NetworkServiceBrowser</tt>.
   *
   * @throws Exception if termination fails.
   */
  public abstract void terminate() throws Exception;

  /**
   * Forces a refresh of the browsers service database.
   * (Note: asynchron, the observers will be notified.)
   */
  public abstract void refresh();

  /**
   * Start or stop periodical refreshing of the browsers
   * service database with the given refresh time.
   * <p/>
   * Note that for Encuentro this is not a requirement, as
   * the browser will track announcements and update the
   * database accordingly.
   * <p/>
   * Observers will be notified of changes.
   *
   * @param b    true if to be started, false otherwise.
   * @param time the refresh time.
   * @return true if periodically refreshing, false otherwise.
   */
  public abstract boolean setPeriodicallyRefreshing(boolean b, int time);

  /**
   * Tests if this <tt>NetworkServiceBrowser</tt> is periodically refreshing
   * its service database.
   *
   * @return true if periodically refreshing, false otherwise.
   */
  public abstract boolean isPeriodicallyRefreshing();

}//class NetworkServicesBrowser
