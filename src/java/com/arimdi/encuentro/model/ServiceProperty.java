/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.model;

/**
 * This interface defines a service property that may be
 * associated with a {@link Service}.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 * @see Service
 */
public interface ServiceProperty
    extends Transportable {

  /**
   * Returns the name of this <tt>ServiceProperty</tt>.
   *
   * @return the name as <tt>String</tt>.
   */
  public String getName();

  /**
   * Returns the value of this <tt>ServiceProperty</tt>.
   *
   * @return the value as <tt>String</tt>.
   */
  public String getValue();

  /**
   * Returns the raw data of the value.
   *
   * @return the value as <tt>byte[]</tt>.
   */
  public byte[] getRawValue();

  /**
   * Returns this <tt>ServiceProperty</tt> in a newly
   * allocated String.<p>
   * The string is composed of:<br>
   * <name>=<value>
   *
   * @return the property as <tt>String</tt>.
   */
  public String toString();

  /**
   * Defines the maximum length of a property as name/value pair.
   */
  public final static int MAX_LENGTH = 254;

  /**
   * Defines the maximum length of a property name.
   */
  public final static int MAX_NAME_LENGTH = 100;

}//interface ServiceProperty
