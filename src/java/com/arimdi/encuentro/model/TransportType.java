/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.model;

/**
 * Class defining the transport type and valid
 * instances.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public final class TransportType {

  private int m_Value;

  protected TransportType(int value) {
    m_Value = value;
  }//TransportType

  public byte getValue() {
    return (byte) m_Value;
  }//getValue

  public String toString() {
    return NAMES[m_Value];
  }//toString

  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof TransportType)) return false;
    final TransportType transportType = (TransportType) o;
    if (m_Value != transportType.m_Value) return false;
    return true;
  }//equals

  public int hashCode() {
    return m_Value;
  }//hashCode

  private static final int I_PACKAGE = 0;
  private static final int I_STREAM = 1;
  private static final int I_ANY = 2;
  private static final String[] NAMES = {"udp", "tcp", "any"};

  public static TransportType getTransportType(int ttype) {
    switch (ttype) {
      case I_PACKAGE:
        return PACKAGE;
      case I_STREAM:
        return STREAM;
      default:
        return ANY;
    }
  }//getTransportType

  /**
   * Defines the package <tt>TransportType</tt>.
   */
  public static final TransportType PACKAGE =
      new TransportType(TransportType.I_PACKAGE);

  /**
   * Defines the stream <tt>TransportType</tt>.
   */
  public static final TransportType STREAM =
      new TransportType(TransportType.I_STREAM);

  /**
   * Defines any transport type.
   */
  public static final TransportType ANY =
      new TransportType(TransportType.I_ANY);

}//class TransportType

