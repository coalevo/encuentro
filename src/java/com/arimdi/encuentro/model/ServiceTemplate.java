/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.model;

/**
 * This interface defines a service template.
 * <p/>
 * Templates are used for service lookups.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public interface ServiceTemplate {

  /**
   * Returns the name of this <tt>ServiceTemplate</tt> instance.
   *
   * @return the name as <tt>String</tt>.
   */
  public String getName();

  /**
   * Sets the name of this <tt>ServiceTemplate</tt> instance.
   *
   * @param name the name as <tt>String</tt>.
   */
  public void setName(String name);

  /**
   * Returns the type of this <tt>ServiceTemplate</tt>.
   *
   * @return the type as <tt>String</tt>.
   */
  public String getType();

  /**
   * Sets the type of this <tt>ServiceTemplate</tt>.
   *
   * @param type the type as <tt>String</tt>.
   */
  public void setType(String type);

  /**
   * Returns the domain of this <tt>ServiceTemplate</tt>.
   *
   * @return the domain as <tt>String</tt>.
   */
  public String getDomain();

  /**
   * Sets the domain of this <tt>ServiceTemplate</tt>.
   *
   * @param domain the domain as <tt>String</tt>.
   */
  public void setDomain(String domain);

  /**
   * Returns the transport type of this <tt>ServiceTemplate</tt>.
   *
   * @return the transport type.
   */
  public TransportType getTransportType();

  /**
   * Sets the transport type of this <tt>ServiceTemplate</tt>.
   *
   * @param ttype the transport type.
   */
  public void setTransportType(TransportType ttype);

  /**
   * Returns the query that specifies service property contents
   * to be matched.
   *
   * @return a query.
   */
  public String getQuery();

  /**
   * Sets the query that specifies service property contents to be
   * matched.
   *
   * @param q the query.
   */
  public void setQuery(String q);

}//interface ServiceTemplate
