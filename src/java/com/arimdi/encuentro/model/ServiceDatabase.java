/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.model;

import java.util.Hashtable;

/**
 * This interface defines a service database for a
 * {@link NetworkServicesBrowser} instance.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 * @see NetworkServicesBrowser
 */
public interface ServiceDatabase {

  /**
   * Tests if this <tt>ServiceDatabase</tt> contains
   * the given service.
   * <p/>
   * It allows lokal lookups and listings of the discovered
   * services.
   *
   * @param s a <tt>Service</tt> instace.
   * @return true if contained, false otherwise.
   */
  public boolean contains(Service s);

  /**
   * Returns a list of services matching the
   * given {@link ServiceTemplate} instance.
   *
   * @param st a <tt>ServiceTemplate</tt> instance.
   * @return an array containing references to matching
   *         services, empty if none matches.
   * @see Service#matches(ServiceTemplate)
   */
  public Service[] findService(ServiceTemplate st);

  /**
   * Returns all services in this <tt>ServiceDatabase</tt>.
   *
   * @return an array containing references to all services.
   */
  public Service[] getServices();

  /**
   * Returns the number of services in this <tt>ServiceDatabase</tt>.
   *
   * @return the number of services as <tt>int</tt>.
   */
  public int size();

  /**
   * Creates a tree index of the database for a browser.
   *
   * @return a <tt>Hashtable</tt>.
   */
  public Hashtable createTreeIndex();

}//interface ServiceDatabase
