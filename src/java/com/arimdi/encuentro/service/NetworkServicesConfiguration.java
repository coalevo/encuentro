/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.service;

import org.osgi.service.cm.ManagedService;

/**
 * Tagging interface that defines constants for a configuration
 * dictionary for Encuentro.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface NetworkServicesConfiguration
    extends ManagedService {

  /**
   * Defines the dictionary key for the debug property.
   * <p/>
   * If debug is <tt>true</tt> (default is <tt>false</tt>) then
   * debug printouts will be send to STDOUT (you may want to
   * redirect it).
   */
  public static final String DEBUG = "com.arimdi.encuentro.debug";

  /**
   * Defines the dictionary key for the announce property.
   * <p/>
   * If announce is <tt>true</tt> (default is <tt>false</tt>) then
   * services will be announced when registered.
   */
  public static final String ANNOUNCE = "com.arimdi.encuentro.announce";

  /**
   * Defines the dictionary key for the secure property.
   * <p/>
   * If set <tt>true</tt> (default is <tt>false</tt>) then the
   * packets will be encrypted with an AES-CBC 128 bit cipher.
   */
  public static final String SECURE = "com.arimdi.encuentro.secure";

  /**
   * Defines the dictionary key for the secure key property.
   * <p/>
   * The key used to derive the AES cipher key.
   */
  public static final String SECURE_KEY = "com.arimdi.encuentro.secure.key";

  /**
   * Defines the dictionary key for the MTU property.
   * <p/>
   * The MTU should be the maximum transport unit supported
   * by the underlying network for multicasts. The default value is
   * 1400, a safe setting for standard Ethernet connections.
   */
  public static final String MTU = "com.arimdi.encuentro.mtu";

}//interface NetworkServicesConfiguration
