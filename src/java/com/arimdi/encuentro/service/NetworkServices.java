/***
 * Coalevo-Arimdi Project
 * http://www.coalevo.net
 *
 * Copyright (c) 2003-2013 Dieter Wimberger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package com.arimdi.encuentro.service;

import com.arimdi.encuentro.model.Service;
import com.arimdi.encuentro.model.ServiceConflict;
import com.arimdi.encuentro.model.ServiceTemplate;

import java.net.InetAddress;

/**
 * Defines a generic interface for service discovery services.
 *
 * @author Dieter Wimberger
 * @version @version@ (@date@)
 */
public interface NetworkServices {

  /**
   * Queries the given server for network services
   * matching the given service template.
   *
   * @param st a <tt>ServiceTemplate</tt> instance describing the service.
   * @param srv an <tt>InetAddress</tt> denoting a DNS server.
   *
   * @return the services discovered or an empty array otherwise.
   * @throws UnsupportedOperationException if the operation is not supported
   *         by the underlying implementation.
   */
   public Service[] query(ServiceTemplate st, InetAddress srv);


  /**
   * Discover a network service matching the given
   * service template.
   *
   * @param st a <tt>ServiceTemplate</tt> instance describing the service.
   * @return the service if discovered, null otherwise.
   */
  public Service discover(ServiceTemplate st);

  /**
   * Discover or wait for a network service matching the given
   * service template.
   *
   * @param st   a <tt>ServiceTemplate</tt> instance describing the service.
   * @param time the time to be waited for the service in ms, or
   *             -1 for endless.
   * @return the service if discovered, null otherwise.
   */
  public Service discover(ServiceTemplate st, int time);

  /**
   * Discover all network services matching the given service
   * template within a given timeframe.
   *
   * @param st   a <tt>ServiceTemplate</tt> instance describing the service.
   * @param time the timeout for the waiting period.
   * @return the services discovered or an empty array otherwise.
   */
  public Service[] discoverAll(ServiceTemplate st, int time);

  /**
   * Synchronously registers a given service.<p>
   *
   * @param s the <tt>Service</tt> instance to be registered.
   * @return true if registered, false if the service instance name is not
   *         unique.
   * @throws ServiceConflict if there is a naming conflict with an existing service.
   */
  public boolean register(Service s)
      throws ServiceConflict;

  /**
   * Unregisters a given service.<p>
   *
   * @param s the <tt>Service</tt> instance to be unregistered.
   */
  public void unregister(Service s);

  /**
   * Unregisters all services that were registered via this
   * <tt>NetworkServices</tt> instance.
   */
  public void unregisterAll();

  /**
   * Tests if this <tt>NetworkServices</tt> instance will
   * announce service register and unregister operations.
   *
   * @return true if announcing, false otherwise.
   * @see #register(Service)
   * @see #unregister(Service)
   */
  public boolean isAnnouncing();

  /**
   * Sets if this <tt>NetworkServices</tt> instance will
   * announce service register and unregister operations.
   *
   * @param b true if registrations should be announced,
   *          false otherwise.
   */
  public void setAnnouncing(boolean b);

  /**
   * Creates and returns a {@link ServiceTemplate} instance to be
   * used for queries.
   *
   * @return a {@link ServiceTemplate} instance.
   */
  public ServiceTemplate createServiceTemplate();

  /**
   * Creates and returns a {@link Service} instance to be used
   * for registering and unregistering a particular service.
   *
   * @return a {@link Service} instance.
   */
  public Service createService();

  /**
   * Utility method that returns the local addresses.
   * @return the local addresses.
   * @throws Exception if the local addresses cannot be obtained.
   */
  public InetAddress[] getLocalAddresses() throws Exception;

  /**
   * Returns the first link local address of this node.
   *
   * @return the first link local address of this node.
   * @throws Exception if the local addresses cannot be obtained.
   */
  public InetAddress getLinkLocalHost() throws Exception;

  /**
   * Returns all link local addresses of this node.
   * @return link local addresses of this node.
   * @throws Exception if the link local addresses cannot be obtained.
   */
  public InetAddress[] getIP4LLAddresses() throws Exception;

}//interface NetworkServices
